/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.services.impl;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.net.Response;
import com.google.common.collect.Lists;
import org.apache.commons.io.IOUtils;

import java.util.Arrays;
import java.util.List;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class RestResponse {

	private final List<String> errors = Lists.newArrayList();
	private int statusCode = -1;
	private boolean successful;
	private String statusMessage;
	private String responseBody;
	private JSONObject json;

	/**
	 * Constructor
	 *
	 * @param response the Response to get the data from
	 */
	public RestResponse(Response response) {
		statusCode = response.getStatusCode();
		statusMessage = response.getStatusText();
		successful = response.isSuccessful();
		try {
			responseBody = IOUtils.toString(response.getResponseBodyAsStream());
			json = new JSONObject(responseBody);
		} catch (JSONException e) {
			json = null;
			// Ignoring this, probably not needed for the request that was send
		} catch (Throwable e) {
			errors.add("Failed to retrieve the response from Jenkins: " + e.getMessage());
		}
	}

	/**
	 * Constructor used to create an Error RestResponse
	 *
	 * @param errors the Error messages
	 */
	public RestResponse(String... errors) {
		this.errors.addAll(Arrays.asList(errors));
	}

	/**
	 * Getter for the status code
	 *
	 * @return the status code
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * Getter for the success state
	 *
	 * @return the success state
	 */
	public boolean isSuccessful() {
		return successful;
	}

	/**
	 * Getter for the status message
	 *
	 * @return the status message
	 */
	public String getStatusMessage() {
		return statusMessage;
	}

	/**
	 * Getter for the response body as a {@link String}
	 * If you're expecting a {@link JSONObject} then you can use the {@link #getJson()}
	 *
	 * @return the response body as a {@link String}
	 * @see #getJson()
	 */
	public String getResponseBody() {
		return responseBody;
	}

	/**
	 * Getter for the response body as an {@link JSONObject}
	 *
	 * @return the response body as a {@link JSONObject}, may be {@code null}
	 */
	public JSONObject getJson() {
		return json;
	}

	/**
	 * Getter for the error collection
	 *
	 * @return the error collection, may be {@code empty}
	 */
	public List<String> getErrors() {
		return errors;
	}

}
