/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.listeners;

import com.atlassian.applinks.api.event.ApplicationLinkDeletedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * {@link EventListener} implementation to update the data on an {@link ApplicationLinkDeletedEvent}
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class JenkinsApplicationLinkDeletedListener extends AbstractJenkinsApplicationLinkListerner {

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsApplicationLinkDeletedListener.class);
	private final SiteService siteService;

	/**
	 * Constructor
	 *
	 * @param eventPublisher the {@link com.atlassian.event.api.EventPublisher} implementation
	 * @param jobService the {@link SiteService} implementation
	 */
	public JenkinsApplicationLinkDeletedListener(EventPublisher eventPublisher, SiteService jobService) {
		super(eventPublisher);
		this.siteService = jobService;
	}

	/**
	 * {@link EventListener} for {@link ApplicationLinkDeletedEvent}
	 * events
	 *
	 * @param event the {@link ApplicationLinkDeletedEvent} event
	 */
	@EventListener
	public void onApplicationLinkDeleted(ApplicationLinkDeletedEvent event) {
		if (supportsApplicationLinkType(event)) {
			LOGGER.info("Marking applink {} [{}] as deleted", event.getApplicationLink().getName(),
					event.getApplicationLink().getDisplayUrl());
			siteService.removeByApplicationLink(event.getApplicationLink());
		} else {
			LOGGER.debug("Skipping applinks event for type {}", event.getApplicationType().getI18nKey());
		}
	}

}
