/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.model;

/**
 * Job State enumeration
 *
 * @author Mark Rekveld
 * @since 1.1.1
 */
public enum JobState {

	IDLE(true), BUILDING(false), QUEUED(false), UNKNOWN(true);

	private final boolean syncable;

	/**
	 * Constructor
	 *
	 * @param syncable flag if a Job is syncable in this state
	 */
	private JobState(boolean syncable) {
		this.syncable = syncable;
	}

	/**
	 * Getter for {@link #syncable}
	 *
	 * @return the syncable state
	 */
	public boolean isSyncable() {
		return syncable;
	}

}
