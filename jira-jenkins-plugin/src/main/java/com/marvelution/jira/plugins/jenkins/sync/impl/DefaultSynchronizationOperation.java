/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.sync.impl;

import com.atlassian.jira.plugins.dvcs.sync.impl.IssueKeyExtractor;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.marvelution.jira.plugins.jenkins.model.*;
import com.marvelution.jira.plugins.jenkins.services.BuildService;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.sync.SynchronizationOperation;
import com.marvelution.jira.plugins.jenkins.sync.SynchronizationOperationException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Set;

/**
 * Default {@link SynchronizationOperation}
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class DefaultSynchronizationOperation implements SynchronizationOperation {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSynchronizationOperation.class);
	private final JobService jobService;
	private final BuildService buildService;
	private final Communicator communicator;
	private final Progress progress;
	private final Job job;

	/**
	 * Constructor
	 *
	 * @param jobService   the {@link JobService}
	 * @param buildService the {@link BuildService}
	 * @param communicator the {@link Communicator}
	 * @param job          the {@link Job}
	 */
	public DefaultSynchronizationOperation(JobService jobService, BuildService buildService,
	                                       Communicator communicator, Job job) {
		this.jobService = jobService;
		this.buildService = buildService;
		this.communicator = communicator;
		this.job = job;
		progress = new Progress();
	}

	@Override
	public void synchronise() throws SynchronizationOperationException {
		int buildCount = 0, issueCount = 0, errorCount = 0;
		if (communicator.isRemoteOnline()) {
			if (!communicator.getJobState(job).isSyncable()) {
				communicator.registerBuildNotifier(job);
				try {
					LOGGER.debug("Synchronizing builds from Job {}", job.getName());
					for (Build build : getBuildsForJobFromRemote(job, job.getLastBuild())) {
						if (progress.isShouldStop()) {
							break;
						}
						if (build.getNumber() > job.getLastBuild()) {
							job.setLastBuild(build.getNumber());
						}
						buildCount++;
						try {
							Build detailedBuild = communicator.getDetailedBuild(job, build);
							LOGGER.debug("Synchronizing Build {} from {}", detailedBuild.getNumber(), job.getName());
							detailedBuild = buildService.save(detailedBuild);
							for (String key : extractIssueKeys(detailedBuild)) {
								key = key.toUpperCase();
								try {
									if (buildService.link(detailedBuild, key)) {
										issueCount++;
									}
								} catch (Exception e) {
									if (progress.isShouldStop()) {
										break;
									}
									LOGGER.warn("Failed to link build {} of {} with issue {}",
											new Object[] { build.getNumber(), job.getName(), key });
								}
							}
						} catch (Exception e) {
							if (progress.isShouldStop()) {
								break;
							}
							LOGGER.warn("Failed to synchronize build {} of {}: {}", new Object[] { build.getNumber(),
									job.getName(), e.getMessage() });
							errorCount++;
						}
						progress.updateProgress(buildCount, issueCount, errorCount);
					}
					jobService.save(job);
				} catch (Exception e) {
					LOGGER.warn("Failed to Synchronize {}: {}", job.getName(), e.getMessage());
				}
			} else {
				LOGGER.info("Skipping synchronisation of {}, its not in a syncable state", job.getName());
			}
		} else {
			LOGGER.info("Skipping synchronisation of {}, remote is not online", job.getName());
		}
	}

	@Override
	public Progress getProgress() {
		return progress;
	}

	/**
	 * Get all the remote builds from the Job given starting from the last build given
	 *
	 * @param job       the {@link Job} to get the builds for
	 * @param lastBuild the last build number
	 * @return collection of {@link Build} objects
	 */
	private Iterable<Build> getBuildsForJobFromRemote(Job job, int lastBuild) {
		List<Build> builds = Lists.newArrayList();
		for (Build build : communicator.getDetailedJob(job).getBuilds()) {
			if (build.getNumber() > lastBuild) {
				builds.add(build);
			}
		}
		return builds;
	}

	/**
	 * Extract all the issue keys from the given {@link Build} It will look for issue keys in the build cause,
	 * change-sets and artifacts
	 *
	 * @param build the {@link Build} to extract the issue keys from
	 * @return the {@link Set} of extracted issue keys
	 */
	private Set<String> extractIssueKeys(Build build) {
		Set<String> keys = Sets.newHashSet();
		keys.addAll(IssueKeyExtractor.extractIssueKeys(build.getCause()));
		for (ChangeSet changeSet : build.getChangeSet()) {
			keys.addAll(IssueKeyExtractor.extractIssueKeys(changeSet.getMessage()));
		}
		LOGGER.debug("Found [{}] related to {}", StringUtils.join(keys, ", "), build.toString());
		return keys;
	}

}
