/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.IssueManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.marvelution.jira.plugins.jenkins.ao.IssueMapping;
import com.marvelution.jira.plugins.jenkins.dao.IssueDao;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;
import com.marvelution.jira.plugins.jenkins.services.impl.BuildIssueFilterQueryBuilder;
import net.java.ao.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Default {@link IssueDao} implementation
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class DefaultIssueDao implements IssueDao {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSiteDao.class);

	private final ActiveObjects ao;
	private final IssueManager issueManager;

	/**
	 * Constructor
	 *
	 * @param ao           the {@link ActiveObjects} implementation
	 * @param issueManager the {@link IssueManager} implementation
	 */
	public DefaultIssueDao(ActiveObjects ao, IssueManager issueManager) {
		this.ao = ao;
		this.issueManager = issueManager;
	}

	@Override
	public Iterable<Integer> getLinksByIssueKey(final String issueKey) {
		List<IssueMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<IssueMapping>>() {
			@Override
			public List<IssueMapping> doInTransaction() {
				IssueMapping[] mappings = ao.find(IssueMapping.class, Query.select().where(IssueMapping.ISSUE_KEY +
						" = ?", issueKey));
				return Arrays.asList(mappings);
			}
		});
		return toBuildIds(mappings);
	}

	@Override
	public Iterable<Integer> getLinksByProjectKey(final String projectKey) {
		List<IssueMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<IssueMapping>>() {
			@Override
			public List<IssueMapping> doInTransaction() {
				IssueMapping[] mappings = ao.find(IssueMapping.class, Query.select().where(IssueMapping.PORJECT_KEY +
						" = ?", projectKey));
				return Arrays.asList(mappings);
			}
		});
		return toBuildIds(mappings);
	}

	@Override
	public Iterable<Integer> getLatestLinksByFilter(final int maxResults, final BuildIssueFilter filter) {
		List<IssueMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<IssueMapping>>() {
			@Override
			public List<IssueMapping> doInTransaction() {
				BuildIssueFilterQueryBuilder queryBuilder = new BuildIssueFilterQueryBuilder(filter);
				Query query = Query.select().where(queryBuilder.build()).order(IssueMapping
						.BUILD_DATE + " DESC");
				if (maxResults != -1) {
					query = query.limit(maxResults);
				}
				IssueMapping[] mappings = ao.find(IssueMapping.class, query);
				return Arrays.asList(mappings);
			}
		});
		return toBuildIds(mappings);
	}

	@Override
	public Iterable<String> getIssueKeysByBuildNumber(final Build build) {
		List<IssueMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<IssueMapping>>() {
			@Override
			public List<IssueMapping> doInTransaction() {
				IssueMapping[] mappings = ao.find(IssueMapping.class, Query.select().where(IssueMapping.BUILD_ID +
						" = ?", build.getId()));
				return Arrays.asList(mappings);
			}
		});
		return transformNoDuplicates(mappings, new Function<IssueMapping, String>() {
			@Override
			public String apply(@Nullable IssueMapping input) {
				return input != null ? input.getIssueKey() : null;
			}
		});
	}

	@Override
	public Iterable<String> getProjectKeysByBuildNumber(final Build build) {
		List<IssueMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<IssueMapping>>() {
			@Override
			public List<IssueMapping> doInTransaction() {
				IssueMapping[] mappings = ao.find(IssueMapping.class, Query.select().where(IssueMapping.BUILD_ID +
						" = ?", build.getId()));
				return Arrays.asList(mappings);
			}
		});
		return transformNoDuplicates(mappings, new Function<IssueMapping, String>() {
			@Override
			public String apply(@Nullable IssueMapping input) {
				return input != null ? input.getProjectKey() : null;
			}
		});
	}

	@Override
	public int getIssueLinkCount(final Build build) {
		return ao.executeInTransaction(new TransactionCallback<Integer>() {
			@Override
			public Integer doInTransaction() {
				return ao.count(IssueMapping.class, Query.select().where(IssueMapping.BUILD_ID + " = ?",
						build.getId()));
			}
		});
	}

	/**
	 * Transform the given {@link List} of {@link IssueMapping} objects to an {@link Iterable} of Build Ids
	 *
	 * @param mappings the {@link IssueMapping}s to transform
	 * @return the {@link Iterable} of build ids
	 */
	private Iterable<Integer> toBuildIds(List<IssueMapping> mappings) {
		return transformNoDuplicates(mappings, new Function<IssueMapping, Integer>() {
			@Override
			public Integer apply(IssueMapping from) {
				return from.getBuildId();
			}
		});
	}

	/**
	 * Transform the given {@link List} of {@link IssueMapping}s using the given {@link Function}
	 *
	 * @param mappings the {@link List} of {@link IssueMapping}s to transform
	 * @param function the transformation {@link Function} to use
	 * @param <I>      the resulting item Type
	 * @return the transformed collection
	 */
	private <I> Iterable<I> transformNoDuplicates(List<IssueMapping> mappings, Function<IssueMapping, I> function) {
		return Sets.newHashSet(Iterables.transform(mappings, function));
	}

	@Override
	public boolean link(final Build build, String issueKey) {
		checkNotNull(build, "build may not be null");
		checkNotNull(issueKey, "issueKey may not be null");
		final Issue issue = issueManager.getIssueObject(issueKey);
		if (issue != null) {
			LOGGER.debug("Linking {} to {}", issue.getKey(), build.toString());
			ao.executeInTransaction(new TransactionCallback<Object>() {
				@Override
				public Object doInTransaction() {
					Map<String, Object> map = Maps.newHashMap();
					map.put(IssueMapping.JOB_ID, build.getJobId());
					map.put(IssueMapping.BUILD_ID, build.getId());
					map.put(IssueMapping.ISSUE_KEY, issue.getKey());
					map.put(IssueMapping.BUILD_DATE, build.getTimestamp());
					map.put(IssueMapping.PORJECT_KEY, issue.getProjectObject().getKey());
					ao.create(IssueMapping.class, map);
					return null;
				}
			});
			return true;
		} else {
			LOGGER.debug("No issue with key {} on this JIRA instance, skipping link", issueKey);
			return false;
		}
	}

}
