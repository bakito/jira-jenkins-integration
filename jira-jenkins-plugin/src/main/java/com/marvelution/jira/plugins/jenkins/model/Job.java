/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.model;

import com.google.common.collect.Lists;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
@XmlRootElement(name = "job")
@XmlAccessorType(XmlAccessType.FIELD)
public class Job {

	@XmlAttribute
	private int id;
	@XmlAttribute
	private int siteId;
	private String name;
	private String description;
	private int lastBuild = 0;
	private boolean linked = false;
	private boolean deleted = false;
	private boolean inQueue = false;
	private boolean buildable = true;
	@XmlElement(name = "build")
	private List<Build> builds;

	/**
	 * Default constructor for JAXB
	 */
	Job() {
	}

	/**
	 * Constructor
	 *
	 * @param siteId the id of the Site the job is located on
	 * @param name   the name of the Job
	 */
	public Job(int siteId, String name) {
		this(0, siteId, name);
	}

	/**
	 * Constructor
	 *
	 * @param id     the ID of the Job
	 * @param siteId the Site Id the job is located on
	 * @param name   the name of the Job
	 */
	public Job(int id, int siteId, String name) {
		this.id = id;
		this.siteId = siteId;
		this.name = name;
	}

	/**
	 * Constructor to copy an original Job given. Only the Id, SiteId and Name are copied!
	 *
	 * @param job the original {@link Job} to copy from
	 */
	public Job(Job job) {
		id = job.getId();
		siteId = job.getSiteId();
		name = job.getName();
	}

	/**
	 * Getter for the id
	 *
	 * @return the Job ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter for the siteId
	 *
	 * @return the siteId
	 */
	public int getSiteId() {
		return siteId;
	}

	/**
	 * Setter for the siteId
	 *
	 * @param siteId the siteId
	 */
	public void setSiteId(int siteId) {
		this.siteId = siteId;
	}

	/**
	 * Getter for the name
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Setter for the name
	 *
	 * @param name the name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Getter for the Job description
	 *
	 * @return the Job description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Setter for the Job description
	 *
	 * @param description the Job description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Getter for the lastBuild
	 *
	 * @return the lastBuild number
	 */
	public int getLastBuild() {
		return lastBuild;
	}

	/**
	 * Setter for the lastBuild
	 *
	 * @param lastBuild the last Build number
	 */
	public void setLastBuild(int lastBuild) {
		this.lastBuild = lastBuild;
	}

	/**
	 * Getter for the linked state
	 *
	 * @return the linked state
	 */
	public boolean isLinked() {
		return linked;
	}

	/**
	 * Setter for the linked state
	 *
	 * @param linked the new linked state
	 */
	public void setLinked(boolean linked) {
		this.linked = linked;
	}

	/**
	 * Getter for the delete state
	 *
	 * @return the deleted state
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * Setter for the deleted state
	 *
	 * @param deleted the new deleted state
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Getter if the job is currently in the remote queue
	 *
	 * @return {@code true} if the job is remotely queued for a build, {@code false} otherwise
	 */
	public boolean isInQueue() {
		return inQueue;
	}

	/**
	 * Setter if the job is currently remote queued
	 *
	 * @param inQueue the current queue state
	 */
	public void setInQueue(boolean inQueue) {
		this.inQueue = inQueue;
	}

	/**
	 * Getter for the buildable state
	 *
	 * @return the buildable state
	 */
	public boolean isBuildable() {
		return buildable;
	}

	/**
	 * Setter if the job is buildable
	 *
	 * @param buildable buildable state
	 */
	public void setBuildable(boolean buildable) {
		this.buildable = buildable;
	}

	/**
	 * Getter for the job {@link Build} {@link List}
	 *
	 * @return the job {@link Build} {@link List}, never {@code null}
	 */
	public List<Build> getBuilds() {
		if (builds == null) {
			builds = Lists.newArrayList();
		}
		return builds;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(id).append(siteId).append(name).build();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof Job) {
			Job other = (Job) o;
			return (id == other.id && siteId == other.siteId) || (siteId == other.siteId && name.equals(other.name));
		}
		return false;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE).append("id", id).append("siteId",
				siteId).append("name", name).build();
	}

}
