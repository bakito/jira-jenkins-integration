/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Build change set
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
@XmlRootElement(name = "change-set")
@XmlAccessorType(XmlAccessType.FIELD)
public class ChangeSet {

	@XmlAttribute
	private String commitId;
	private String message;

	/**
	 * Default constructor for JAXB
	 */
	ChangeSet() {
	}

	/**
	 * Constructor
	 *
	 * @param commitId the commit Id
	 * @param message  the commit message
	 */
	public ChangeSet(String commitId, String message) {
		this.commitId = commitId;
		this.message = message;
	}

	/**
	 * Getter for the Commit Id
	 *
	 * @return the Commit Id
	 */
	public String getcommitId() {
		return commitId;
	}

	/**
	 * Getter for the commit message
	 *
	 * @return the commit message
	 */
	public String getMessage() {
		return message;
	}

}
