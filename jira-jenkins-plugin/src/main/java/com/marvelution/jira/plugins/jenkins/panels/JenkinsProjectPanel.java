/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.panels;

import com.atlassian.jira.plugin.projectpanel.impl.AbstractProjectTabPanel;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.security.Permissions;
import com.marvelution.jira.plugins.jenkins.model.Build;

import java.util.List;
import java.util.Map;

/**
 * The {@link AbstractProjectTabPanel} implementation to get all the {@link Build}s related to the {@link
 * BrowseContext}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JenkinsProjectPanel extends AbstractProjectTabPanel {

	private final PermissionManager permissionManager;
	private final BuildPanelHelper buildPanelHelper;

	/**
	 * Constructor
	 *
	 * @param permissionManager the {@link PermissionManager} implementation
	 * @param buildPanelHelper the {@link BuildPanelHelper} implementation
	 */
	public JenkinsProjectPanel(PermissionManager permissionManager, BuildPanelHelper buildPanelHelper) {
		this.permissionManager = permissionManager;
		this.buildPanelHelper = buildPanelHelper;
	}

	@Override
	protected Map<String, Object> createVelocityParams(BrowseContext ctx) {
		Map<String, Object> params = super.createVelocityParams(ctx);
		Iterable<? extends Build> builds = buildPanelHelper.getBuildsByRelation(ctx.getProject());
		List<BuildAction> buildActions = buildPanelHelper.getBuildActions(builds);
		params.put("actions", buildActions);
		return params;
	}

	@Override
	public boolean showPanel(BrowseContext ctx) {
		return permissionManager.hasPermission(Permissions.VIEW_VERSION_CONTROL, ctx.getProject(), ctx.getUser());
	}

}
