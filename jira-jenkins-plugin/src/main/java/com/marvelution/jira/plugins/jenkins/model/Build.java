/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.model;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

import javax.xml.bind.annotation.*;
import java.util.Date;
import java.util.List;

/**
 * Build model
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
@XmlRootElement(name = "build")
@XmlAccessorType(XmlAccessType.FIELD)
public class Build {

	@XmlAttribute
	private int id;
	@XmlAttribute
	private int jobId;
	private int number;
	private boolean deleted;
	private String cause;
	private String result;
	private String builtOn;
	private long duration;
	private long timestamp;
	@XmlElement(name = "artifact")
	private List<Artifact> artifacts;
	@XmlElement(name = "culprit")
	private List<Culprit> culprits;
	@XmlElement(name = "change-set")
	private List<ChangeSet> changeSet;

	/**
	 * Default constructor for JAXB
	 */
	Build() {
		id = 0;
	}

	/**
	 * Constructor
	 *
	 * @param jobId  the jobId the build belongs to
	 * @param number the build number
	 */

	public Build(int jobId, int number) {
		this(0, jobId, number);
	}

	/**
	 * Constructor
	 *
	 * @param id     the id of the build
	 * @param jobId  the jobId the build belongs to
	 * @param number the build number
	 */

	public Build(int id, int jobId, int number) {
		this.id = id;
		this.jobId = jobId;
		this.number = number;
	}

	/**
	 * Getter for the ID
	 *
	 * @return the ID
	 */
	public int getId() {
		return id;
	}

	/**
	 * Getter for jobId
	 *
	 * @return the jobId
	 */
	public int getJobId() {
		return jobId;
	}

	/**
	 * Getter for the build number
	 *
	 * @return the build number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Getter for the build deleted state
	 *
	 * @return the build deleted state
	 */
	public boolean isDeleted() {
		return deleted;
	}

	/**
	 * Setter for the build deleted state
	 *
	 * @param deleted the build deleted state
	 */
	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	/**
	 * Getter for the cause
	 *
	 * @return the cause
	 */
	public String getCause() {
		return cause;
	}

	/**
	 * Setter for the cause
	 *
	 * @param cause the cause
	 */
	public void setCause(String cause) {
		this.cause = cause;
	}

	/**
	 * Getter for the build result
	 *
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * Setter for the build result
	 *
	 * @param result the build result
	 */
	public void setResult(String result) {
		this.result = result;
	}

	/**
	 * Getter for the name of the build agent
	 *
	 * @return the name of the build agent
	 */
	public String getBuiltOn() {
		return builtOn;
	}

	/**
	 * Setter for the name of the build agent
	 *
	 * @param builtOn name of the build agent
	 */
	public void setBuiltOn(String builtOn) {
		this.builtOn = builtOn;
	}

	/**
	 * Getter for the build duration
	 *
	 * @return the build duration
	 */
	public long getDuration() {
		return duration;
	}

	/**
	 * Setter for the build duration
	 *
	 * @param duration the build duration
	 */
	public void setDuration(long duration) {
		this.duration = duration;
	}

	/**
	 * Getter for the build {@link Date}
	 *
	 * @return the build {@link Date}, may be {@code null} in case the {@link #timestamp} is {@code 0}
	 */
	public Date getBuildDate() {
		if (timestamp > 0) {
			return new Date(timestamp);
		} else {
			return null;
		}
	}

	/**
	 * Getter for the build timestamp
	 *
	 * @return the build timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * Setter for the build timestamp
	 *
	 * @param timestamp the build timestamp
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * Getter for the {@link Artifact} {@link List}
	 *
	 * @return the {@link Artifact} {@link List}, never {@link null}
	 */
	public List<Artifact> getArtifacts() {
		if (artifacts == null) {
			artifacts = Lists.newArrayList();
		}
		return artifacts;
	}

	/**
	 * Getter for the {@link Culprit} {@link List}
	 *
	 * @return the {@link Culprit} {@link List}, never {@link null}
	 */
	public List<Culprit> getCulprits() {
		if (culprits == null) {
			culprits = Lists.newArrayList();
		}
		return culprits;
	}

	/**
	 * Getter for the {@link ChangeSet} {@link List}
	 *
	 * @return the {@link ChangeSet} {@link List}, never {@link null}
	 */
	public List<ChangeSet> getChangeSet() {
		if (changeSet == null) {
			changeSet = Lists.newArrayList();
		}
		return changeSet;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this).add("id", id).add("jobId", jobId).add("number", number).toString();
	}

}
