/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.jql;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.JiraDataType;
import com.atlassian.jira.JiraDataTypes;
import com.atlassian.jira.jql.operand.QueryLiteral;
import com.atlassian.jira.jql.query.QueryCreationContext;
import com.atlassian.jira.plugin.jql.function.AbstractJqlFunction;
import com.atlassian.jira.util.MessageSet;
import com.atlassian.jira.util.MessageSetImpl;
import com.atlassian.jira.util.NotNull;
import com.atlassian.query.clause.TerminalClause;
import com.atlassian.query.operand.FunctionOperand;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.services.BuildService;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * JQL Function to located issues related to a Jenkins Build
 *
 * @author Mark Rekveld
 * @since 1.1.1
 */
public class IssuesRelatedToBuildFunction extends AbstractJqlFunction {

	private static final Logger LOGGER = LoggerFactory.getLogger(IssuesRelatedToBuildFunction.class);
	private final BuildService buildService;
	private final JobService jobService;

	/**
	 * Constructor
	 *
	 * @param buildService the {@link com.marvelution.jira.plugins.jenkins.services.BuildService} implementation
	 * @param jobService   the {@link com.marvelution.jira.plugins.jenkins.services.JobService} implementation
	 */
	public IssuesRelatedToBuildFunction(BuildService buildService, JobService jobService) {
		this.buildService = buildService;
		this.jobService = jobService;
	}

	@Override
	public JiraDataType getDataType() {
		return JiraDataTypes.ISSUE;
	}

	@Override
	public int getMinimumNumberOfExpectedArguments() {
		return 1;
	}

	@Override
	public MessageSet validate(User user, @NotNull FunctionOperand functionOperand,
	                           @NotNull TerminalClause terminalClause) {
		MessageSet messages = new MessageSetImpl();
		final List<String> arguments = functionOperand.getArgs();
		if (arguments.isEmpty()) {
			messages.addErrorMessage(getI18n().getText("jql.invalid.number.of.arguments", 1));
		} else {
			switch (arguments.size()) {
				case 2:
					if (jobService.get(arguments.get(0)).isEmpty()) {
						messages.addErrorMessage(getI18n().getText("jql.unknown.job", arguments.get(0)));
					}
					try {
						Integer.parseInt(arguments.get(1));
					} catch (NumberFormatException e) {
						messages.addErrorMessage(getI18n().getText("jql.invalid.build.number", arguments.get(1)));
					}
					break;
				case 1:
				default:
					// Only a single argument, assume its the Build Id and validate that
					try {
						final int buildId = Integer.parseInt(arguments.get(0));
						if (buildService.get(buildId) == null) {
							messages.addErrorMessage(getI18n().getText("jql.unknown.build", arguments.get(0)));
						}
					} catch (NumberFormatException e) {
						messages.addErrorMessage(getI18n().getText("jql.invalid.build.id", arguments.get(0)));
					}
					break;
			}
		}
		return messages;
	}

	@Override
	public List<QueryLiteral> getValues(@NotNull QueryCreationContext queryCreationContext,
	                                    @NotNull FunctionOperand functionOperand,
	                                    @NotNull TerminalClause terminalClause) {
		List<QueryLiteral> literals = Lists.newArrayList();
		List<String> arguments = functionOperand.getArgs();
		List<Build> builds = Lists.newArrayList();
		switch (arguments.size()) {
			case 2:
				try {
					final List<Job> jobs = jobService.get(arguments.get(0));
					final int buildNumber = Integer.parseInt(arguments.get(1));
					for (Job job : jobs) {
						builds.add(buildService.get(job, buildNumber));
					}
				} catch (Exception e) {
					LOGGER.debug("Failed to get build {} of job {}, {}", new Object[] {arguments.get(1),
							arguments.get(1), e.getMessage()});
				}
				break;
			case 1:
			default:
				// Only a single argument, assume its the Build Id and validate that
				try {
					final int buildId = Integer.parseInt(arguments.get(0));
					builds.add(buildService.get(buildId));
				} catch (Exception e) {
					LOGGER.debug("Failed to get build {}, {}", new Object[] {arguments.get(0), e.getMessage()});
				}
				break;
		}
		for (Build build : builds) {
			if (build != null) {
				for (String issueKey : buildService.getRelatedIssueKeys(build)) {
					literals.add(new QueryLiteral(functionOperand, issueKey));
				}
			}
		}
		return literals;
	}

}
