/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.services.impl;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.config.properties.ApplicationProperties;
import com.marvelution.jira.plugins.jenkins.applinks.HudsonApplicationType;
import com.marvelution.jira.plugins.jenkins.applinks.JenkinsApplicationType;
import com.marvelution.jira.plugins.jenkins.dao.SiteDao;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Default implementation of {@link CommunicatorFactory}
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class DefaultCommunicatorFactory implements CommunicatorFactory {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultCommunicatorFactory.class);
	private final ApplicationLinkService applicationLinkService;
	private final ApplicationProperties applicationProperties;
	private final SiteDao siteDao;

	/**
	 * Constructor
	 *
	 * @param applicationLinkService the {@link com.atlassian.applinks.api.ApplicationLinkService} implementation
	 * @param applicationProperties  the {@link com.atlassian.jira.config.properties.ApplicationProperties} implementation
	 * @param siteDao                the {@link com.marvelution.jira.plugins.jenkins.dao.SiteDao} implementation
	 */
	public DefaultCommunicatorFactory(ApplicationLinkService applicationLinkService,
	                                  ApplicationProperties applicationProperties, SiteDao siteDao) {
		this.applicationLinkService = applicationLinkService;
		this.applicationProperties = applicationProperties;
		this.siteDao = siteDao;
	}

	@Override
	public Communicator get(int siteId) {
		return get(siteDao.get(siteId));
	}

	@Override
	public Communicator get(Site site) {
		checkNotNull(site);
		return getByApplicationId(site, site.getApplicationId());
	}

	@Override
	public Communicator get(ApplicationId applicationId) {
		checkNotNull(applicationId);
		return getByApplicationId(siteDao.get(applicationId), applicationId);
	}

	@Override
	public Communicator get(ApplicationLink applicationLink) {
		checkNotNull(applicationLink);
		return getByApplicationLink(siteDao.get(applicationLink.getId()), applicationLink);
	}

	/**
	 * Get a {@link Communicator} instance for the given {@link Site} and {@link ApplicationId} given
	 *
	 * @param site          the {@link Site}
	 * @param applicationId the {@link ApplicationId}
	 * @return teh {@link Communicator} instance
	 */
	private Communicator getByApplicationId(Site site, ApplicationId applicationId) {
		checkNotNull(applicationId);
		try {
			return getByApplicationLink(site, applicationLinkService.getApplicationLink(applicationId));
		} catch (TypeNotInstalledException e) {
			throw new IllegalArgumentException("Unable to get an ApplicationLink for " + applicationId);
		}
	}

	/**
	 * Get a {@link Communicator} instance for the given {@link Site} and {@link ApplicationLink} given
	 *
	 * @param site            the {@link Site}
	 * @param applicationLink the {@link ApplicationLink}
	 * @return teh {@link Communicator} instance
	 */
	private Communicator getByApplicationLink(Site site, ApplicationLink applicationLink) {
		checkNotNull(site);
		checkNotNull(applicationLink);
		LOGGER.debug("Getting a Communicator for ApplicationLink '" + applicationLink.getName() + "' of type " +
				applicationLink.getType().getI18nKey());
		if (applicationLink.getType() instanceof JenkinsApplicationType || applicationLink.getType() instanceof
				HudsonApplicationType) {
			return new RestCommunicator(applicationProperties.getString(APKeys.JIRA_BASEURL), site, applicationLink);
		} else {
			throw new IllegalArgumentException(applicationLink.getName() + " is not a Jenkins Application Link");
		}
	}

}
