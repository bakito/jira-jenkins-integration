/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.dao;

import com.marvelution.jira.plugins.jenkins.model.Build;
import com.marvelution.jira.plugins.jenkins.services.BuildIssueFilter;

/**
 * Issue Data Access interface
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public interface IssueDao {

	/**
	 * Get all the build ids of links by the given issue key
	 *
	 * @param issueKey the issue key to get all the links for
	 * @return collection of all the build ids the given issue key links with, may be {@code empty} but not {@code
	 * null}
	 */
	Iterable<Integer> getLinksByIssueKey(String issueKey);

	/**
	 * Get all the build ids of links by the given project key
	 *
	 * @param projectKey the project key to get all the links for
	 * @return collection of all the build ids the given project key links with, may be {@code empty} but not {@code
	 * null}
	 */
	Iterable<Integer> getLinksByProjectKey(String projectKey);

	/**
	 * Get all teh build ids that match the given {@link BuildIssueFilter}
	 *
	 * @param maxResults the maximum number of results
	 * @param filter the {@link BuildIssueFilter}
	 * @return collection of all the matching build ids
	 */
	Iterable<Integer> getLatestLinksByFilter(int maxResults, BuildIssueFilter filter);

	/**
	 * Get all the related Issue Keys for the {@link Build} given
	 *
	 * @param build the {@link Build} to get the issue keys for
	 * @return the collection of all the related issue keys
	 */
	Iterable<String> getIssueKeysByBuildNumber(Build build);

	/**
	 * Get all the related Project Keys for the {@link Build} given
	 *
	 * @param build the {@link Build} to get the project keys for
	 * @return the collection of all the related project keys
	 */
	Iterable<String> getProjectKeysByBuildNumber(Build build);

	/**
	 * Get the count of {@link com.marvelution.jira.plugins.jenkins.ao.IssueMapping}s for the given {@link Build}
	 *
	 * @param build the {@link Build}
	 * @return the count, zero or higher
	 */
	int getIssueLinkCount(Build build);

	/**
	 * Create a new link between the given {@link Build} and issue key
	 *
	 * @param build the {@link Build}
	 * @param issueKey the issue key to link with
	 * @return {@code true} in case of a successful link, {@code false} otherwise
	 */
	boolean link(Build build, String issueKey);

}
