/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.rest.filter;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.google.common.collect.Lists;
import com.sun.jersey.api.model.AbstractMethod;
import com.sun.jersey.spi.container.ResourceFilter;
import com.sun.jersey.spi.container.ResourceFilterFactory;

import javax.ws.rs.ext.Provider;
import java.util.List;

/**
 * {@link Provider} for the {@link ResourceFilterFactory}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@Provider
public class AdminRequiredResourceFilterFactory implements ResourceFilterFactory {

	private final JiraAuthenticationContext authenticationContext;
	private final PermissionManager permissionManager;

	/**
	 * Constructor
	 *
	 * @param authenticationContext the {@link JiraAuthenticationContext}
	 * @param permissionManager     the {@link PermissionManager}
	 */
	public AdminRequiredResourceFilterFactory(JiraAuthenticationContext authenticationContext,
	                                          PermissionManager permissionManager) {
		this.authenticationContext = authenticationContext;
		this.permissionManager = permissionManager;
	}

	@Override
	public List<ResourceFilter> create(AbstractMethod abstractMethod) {
		return Lists.<ResourceFilter>newArrayList(new AdminRequiredResourceFilter(abstractMethod,
				authenticationContext, permissionManager));
	}

}
