/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.sync.impl;

import com.google.common.collect.MapMaker;
import com.marvelution.jira.plugins.jenkins.model.Job;
import com.marvelution.jira.plugins.jenkins.model.Progress;
import com.marvelution.jira.plugins.jenkins.sync.SynchronizationOperation;
import com.marvelution.jira.plugins.jenkins.sync.Synchronizer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import javax.annotation.PreDestroy;
import java.util.Map;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;

/**
 * Synchronization Service
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class DefaultSynchronizer implements Synchronizer, DisposableBean {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSynchronizer.class);
	private final ConcurrentMap<Job, Progress> progressMap = new MapMaker().makeMap();
	private final ExecutorService executorService;

	/**
	 * Constructor
	 *
	 * @param executorService the {@link ExecutorService}
	 */
	public DefaultSynchronizer(ExecutorService executorService) {
		this.executorService = executorService;
	}

	@Override
	public void synchronize(Job job, SynchronizationOperation operation) {
		Progress progress = progressMap.get(job);
		if (progress == null || progress.isFinished() || progress.isShouldStop()) {
			LOGGER.debug("Scheduling synchronization for {}", job);
			synchronizeInternal(job, operation);
		}
	}

	/**
	 * Add a {@link SynchronizationOperation} to the Queue on the {@link #executorService}
	 *
	 * @param job       the {@link Job} for which the {@link SynchronizationOperation} is being queued
	 * @param operation the {@link SynchronizationOperation} to perform
	 */
	private void synchronizeInternal(Job job, final SynchronizationOperation operation) {
		final Progress progress = operation.getProgress();
		progressMap.put(job, progress);
		Runnable runnable = new Runnable() {
			/**
			 * {@inheritDoc}
			 */
			@Override
			public void run() {
				try {
					progress.start();
					if (progress.isShouldStop()) {
						return;
					}
					operation.synchronise();
				} catch (Throwable e) {
					String message = (e.getMessage() != null ? e.getMessage() : e.toString());
					progress.setError(message);
					LOGGER.warn(message, e);
				} finally {
					progress.finish();
				}
			}
		};
		executorService.submit(runnable);
		progress.queued();
	}

	@Override
	public void stopSynchronization(Job job) {
		Progress progress = progressMap.get(job);
		if (progress != null) {
			LOGGER.debug("Requesting synchronization stop for {}", job);
			progress.setShouldStop(true);
		}
	}

	@Override
	public Progress getProgress(Job job) {
		return progressMap.get(job);
	}

	/**
	 * Cleanup just before the bean is set to be destroyed.
	 * This will trigger an all-stop for all ongoing synchronization jobs without logging errors
	 */
	@PreDestroy
	@Override
	public void destroy() throws Exception {
		for (Map.Entry<Job, Progress> entry : progressMap.entrySet()) {
			LOGGER.debug("Requesting synchronization stop for {}", entry.getKey());
			entry.getValue().setShouldStop(true);
		}
	}

}
