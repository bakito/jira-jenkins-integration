/*
 * Licensed to Marvelution under one or more contributor license 
 * agreements.  See the NOTICE file distributed with this work 
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.utils;

import com.atlassian.plugin.osgi.util.OsgiHeaderUtil;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.Version;

import java.net.MalformedURLException;
import java.net.URI;
import java.util.Dictionary;

/**
 * Default Plugin Utility implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class DefaultJenkinsPluginUtil implements JenkinsPluginUtil {

	private final String pluginKey;
	private final Version version;
	private final URI jenkinsPluginUrl;

	/**
	 * Default constructor
	 *
	 * @param bundleContext the {@link BundleContext}
	 */
	public DefaultJenkinsPluginUtil(BundleContext bundleContext) throws MalformedURLException {
		Bundle bundle = bundleContext.getBundle();
		pluginKey = OsgiHeaderUtil.getPluginKey(bundle);
		version = bundle.getVersion();
		Dictionary headers = bundle.getHeaders();
		jenkinsPluginUrl = URI.create(headers.get("Jenkins-PluginUrl").toString());
	}

	@Override
	public String getPluginKey() {
		return pluginKey;
	}

	@Override
	public Version getVersion() {
		return version;
	}

	@Override
	public URI getJenkinsPluginUrl() {
		return jenkinsPluginUrl;
	}

}
