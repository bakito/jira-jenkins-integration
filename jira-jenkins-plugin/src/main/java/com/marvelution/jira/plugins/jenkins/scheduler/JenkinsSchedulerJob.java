/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.scheduler;

import java.util.Map;

import com.atlassian.sal.api.scheduling.PluginJob;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.services.CommunicatorFactory;
import com.marvelution.jira.plugins.jenkins.services.JobService;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Jenkins Scheduled Job
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class JenkinsSchedulerJob implements PluginJob {

	private static final Logger LOGGER = LoggerFactory.getLogger(JenkinsSchedulerJob.class);

	public final static String SITE_SERVICE = "site-service";
	public static final String JOB_SERVICE = "job-service";
	public static final String COMMUNICATOR_FACTORY = "communicator-factory";

	@Override
	public void execute(Map<String, Object> data) {
		try {
			final SiteService siteService = (SiteService) data.get(SITE_SERVICE);
			final JobService jobService = (JobService) data.get(JOB_SERVICE);
			final CommunicatorFactory communicatorFactory = (CommunicatorFactory) data.get(COMMUNICATOR_FACTORY);
			for (Site site : siteService.getAll(false)) {
				Communicator communicator = communicatorFactory.get(site);
				site.setSupportsBackLink(communicator.isJenkinsPluginInstalled());
				siteService.save(site);
				jobService.syncJobList(site);
			}
		} catch (Throwable t) {
			LOGGER.warn("Failed to execute the Jenkins Scheduled Job; {}", t.getMessage());
		}
	}

}
