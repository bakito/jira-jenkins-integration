/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.dao.impl;

import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.jira.plugins.dvcs.dao.impl.MapRemovingNullCharacterFromStringValues;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.ao.JobMapping;
import com.marvelution.jira.plugins.jenkins.dao.JobDao;
import com.marvelution.jira.plugins.jenkins.model.Job;
import net.java.ao.Query;

import javax.annotation.Nullable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * The Default implementation of the {@link JobDao} interface
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class DefaultJobDao implements JobDao {

	private final ActiveObjects ao;
	private final Function<JobMapping, Job> jobMappingToJobFunction = new Function<JobMapping, Job>() {
		@Override
		public Job apply(@Nullable JobMapping from) {
			if (from == null) {
				return null;
			}
			Job job = new Job(from.getID(), from.getSiteId(), from.getName());
			job.setLinked(from.isLinked());
			job.setLastBuild(from.getLastBuild());
			return job;
		}
	};

	/**
	 * Constructor
	 *
	 * @param ao the {@link ActiveObjects} implementation
	 */
	public DefaultJobDao(ActiveObjects ao) {
		this.ao = ao;
	}

	@Override
	public List<Job> getAllBySiteId(final int siteId, final boolean includeDeleted) {
		List<JobMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<JobMapping>>() {
			@Override
			public List<JobMapping> doInTransaction() {
				Query query = Query.select();
				if (includeDeleted) {
					query.where(JobMapping.SITE_ID + " = ?", siteId);
				} else {
					query.where(JobMapping.SITE_ID + " = ? AND " + JobMapping.DELETED + " = ?", siteId, Boolean.FALSE);
				}
				query.order(JobMapping.NAME);
				return Arrays.asList(ao.find(JobMapping.class, query));
			}
		});
		return Lists.transform(mappings, jobMappingToJobFunction);
	}

	@Override
	public List<Job> getAll(final boolean includeDeleted) {
		List<JobMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<JobMapping>>() {
			@Override
			public List<JobMapping> doInTransaction() {
				Query query = Query.select();
				if (!includeDeleted) {
					query.where(JobMapping.DELETED + " = ?", Boolean.FALSE);
				}
				query.order(JobMapping.NAME);
				return Arrays.asList(ao.find(JobMapping.class, query));
			}
		});
		return Lists.transform(mappings, jobMappingToJobFunction);
	}

	@Override
	public Job save(final Job job) {
		JobMapping mapping = ao.executeInTransaction(new TransactionCallback<JobMapping>() {
			@Override
			public JobMapping doInTransaction() {
				JobMapping mapping;
				if (job.getId() == 0) {
					Map<String, Object> map = new MapRemovingNullCharacterFromStringValues();
					map.put(JobMapping.SITE_ID, job.getSiteId());
					map.put(JobMapping.NAME, job.getName());
					map.put(JobMapping.LAST_BUILD, job.getLastBuild());
					map.put(JobMapping.LINKED, job.isLinked());
					map.put(JobMapping.DELETED, job.isDeleted());
					mapping = ao.create(JobMapping.class, map);
					mapping = ao.find(JobMapping.class, Query.select().where("ID = ?", mapping.getID()))[0];
				} else {
					mapping = ao.get(JobMapping.class, job.getId());
					mapping.setSiteId(job.getSiteId());
					mapping.setName(job.getName());
					mapping.setLastBuild(job.getLastBuild());
					mapping.setLinked(job.isLinked());
					mapping.setDeleted(job.isDeleted());
					mapping.save();
				}
				return mapping;
			}
		});
		return jobMappingToJobFunction.apply(mapping);
	}

	@Override
	public void remove(int jobId) {
		ao.delete(ao.get(JobMapping.class, jobId));
	}

	@Override
	public void removeAllBySiteId(final int siteId) {
		ao.executeInTransaction(new TransactionCallback<Object>() {
			@Override
			public Object doInTransaction() {
				JobMapping[] mappings = ao.find(JobMapping.class, Query.select().where(JobMapping.SITE_ID + " = ?",
						siteId));
				ao.delete(mappings);
				return null;
			}
		});
	}

	@Override
	public void delete(Job job) {
		job.setDeleted(true);
		save(job);
	}

	@Override
	public Job get(final int jobId) {
		JobMapping mapping = ao.executeInTransaction(new TransactionCallback<JobMapping>() {
			@Override
			public JobMapping doInTransaction() {
				return ao.get(JobMapping.class, jobId);
			}
		});
		return jobMappingToJobFunction.apply(mapping);
	}

	@Override
	public List<Job> get(final String name) {
		List<JobMapping> mappings = ao.executeInTransaction(new TransactionCallback<List<JobMapping>>() {
			@Override
			public List<JobMapping> doInTransaction() {
				return Arrays.asList(ao.find(JobMapping.class, Query.select().where(JobMapping.NAME + " = ?", name)));
			}
		});
		return Lists.transform(mappings, jobMappingToJobFunction);
	}

}
