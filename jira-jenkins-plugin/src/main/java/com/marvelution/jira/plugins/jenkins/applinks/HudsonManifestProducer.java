/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.applinks;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.auth.AuthenticationProvider;
import com.atlassian.applinks.api.auth.types.BasicAuthenticationProvider;
import com.atlassian.applinks.spi.Manifest;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.applinks.spi.manifest.ManifestRetriever;
import com.atlassian.sal.api.net.*;
import com.google.common.collect.ImmutableSet;
import org.osgi.framework.Version;

import java.net.URI;
import java.util.Collections;
import java.util.Set;

/**
 * AppLinksManifestProducer for Hudson applications
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class HudsonManifestProducer extends JenkinsManifestProducer {

	/**
	 * Constructor
	 *
	 * @param requestFactory the {@link com.atlassian.sal.api.net.RequestFactory}
	 * @param manifestRetriever the {@link ManifestRetriever}
	 */
	protected HudsonManifestProducer(RequestFactory<Request<Request<?, Response>, Response>> requestFactory,
	                                 ManifestRetriever manifestRetriever) {
		super(requestFactory, manifestRetriever);
	}

	/**
	 * Create a {@link com.atlassian.applinks.spi.Manifest} for the {@link java.net.URI} given
	 *
	 * @param url the {@link java.net.URI} to create a {@link com.atlassian.applinks.spi.Manifest} for
	 * @return the created {@link com.atlassian.applinks.spi.Manifest}
	 */
	@Override
	protected Manifest createManifest(final URI url) {
		return new Manifest() {

			@Override
			public ApplicationId getId() {
				return ApplicationIdUtil.generate(url);
			}

			@Override
			public String getName() {
				return "Hudson";
			}

			@Override
			public TypeId getTypeId() {
				return HudsonApplicationType.TYPE_ID;
			}

			@Override
			public String getVersion() {
				return null;
			}

			@Override
			public Long getBuildNumber() {
				return 0L;
			}

			@Override
			public URI getUrl() {
				return url;
			}

			@Override
			public Version getAppLinksVersion() {
				return null;
			}

			@Override
			public Boolean hasPublicSignup() {
				return null;
			}

			@Override
			public Set<Class<? extends AuthenticationProvider>> getInboundAuthenticationTypes() {
				return ImmutableSet.<Class<? extends AuthenticationProvider>>of(BasicAuthenticationProvider.class);
			}

			@Override
			public Set<Class<? extends AuthenticationProvider>> getOutboundAuthenticationTypes() {
				return Collections.emptySet();
			}

		};
	}

}
