/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.services.impl;

import com.atlassian.applinks.api.*;
import org.apache.commons.lang3.StringUtils;
import com.atlassian.jira.util.json.JSONArray;
import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.google.common.collect.Lists;
import com.marvelution.jira.plugins.jenkins.model.*;
import com.marvelution.jira.plugins.jenkins.services.Communicator;
import com.marvelution.jira.plugins.jenkins.utils.URIUtils;
import org.apache.commons.httpclient.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.URI;
import java.util.List;

/**
 * JSON Rest Implementation of the {@link com.marvelution.jira.plugins.jenkins.services.Communicator}
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class RestCommunicator implements Communicator {

	public static final int CALL_TIMEOUT = 50000;
	private static final Logger LOGGER = LoggerFactory.getLogger(RestCommunicator.class);
	private final String jiraBaseUrl;
	private final Site site;
	private final ApplicationLink applicationLink;

	private static final String[] COMMIT_ID_FIELDS = new String[] {
			"commitId", // Default
			"id", // GIT
			"node", // Mercurial
			"revision", // Subversion
			"changeItem" // Perforce
	};

	/**
	 * Constructor
	 *
	 * @param jiraBaseUrl     the JIRA base URL
	 * @param site            the {@link com.marvelution.jira.plugins.jenkins.model.Site}
	 * @param applicationLink the {@link com.atlassian.applinks.api.ApplicationLink}
	 */
	public RestCommunicator(String jiraBaseUrl, Site site, ApplicationLink applicationLink) {
		this.jiraBaseUrl = StringUtils.stripEnd(jiraBaseUrl, "/");
		this.site = site;
		this.applicationLink = applicationLink;
	}

	@Override
	public boolean isRemoteOnline() {
		try {
			return isRemoteOnlineAndAccessible();
		} catch (CredentialsRequiredException e) {
			// We only care if the remote application is online or not, not about any auth
			return true;
		}
	}

	@Override
	public boolean isRemoteOnlineAndAccessible() throws CredentialsRequiredException {
		RestResponse response = executeGetRequest("/api/json", CALL_TIMEOUT);
		return response.isSuccessful();
	}

	@Override
	public boolean isJenkinsPluginInstalled() {
		try {
			ApplicationLinkRequestFactory requestFactory = applicationLink.createAuthenticatedRequestFactory();
			ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.POST,
					"/plugin/jenkins-jira-plugin/");
			RestResponse response = executeRequest(request, requestFactory, CALL_TIMEOUT);
			LOGGER.debug("Backlink support check on {}; code: {}", applicationLink.getName(),
					response.getStatusCode());
			return response.getStatusCode() / 100 == 2;
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
					applicationLink.getDisplayUrl(), e.getMessage() });
		}
		return false;
	}

	@Override
	public void registerBuildNotifier(Job job) {
		if (site.isSupportsBackLink()) {
			try {
				String url = "/job/" + URIUtils.encode(job.getName(), "UTF-8") + "/jira/";
				ApplicationLinkRequestFactory requestFactory = applicationLink.createAuthenticatedRequestFactory();
				ApplicationLinkRequest request = requestFactory.createRequest(Request.MethodType.POST, url);
				request.addRequestParameters("postUrl", jiraBaseUrl + "/rest/jenkins/1.0/job/" + job.getId() + "/sync");
				RestResponse response = executeRequest(request, requestFactory, CALL_TIMEOUT);
				if (response.getStatusCode() / 100 != 2) {
					LOGGER.warn("Unable to register build notifier on {}:{}, {} [{}]", new Object[] { applicationLink
							.getName(), job.getName(), response.getStatusMessage(), response.getStatusCode() });
				}
			} catch (UnsupportedEncodingException e) {
				LOGGER.error("Failed to encode the job name: " + job.getName());
			} catch (CredentialsRequiredException e) {
				LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
						applicationLink.getDisplayUrl(), e.getMessage() });
			}
		}
	}

	@Override
	public List<Job> getJobs() {
		List<Job> jobs = Lists.newArrayList();
		try {
			RestResponse response = executeGetRequest("/api/json", CALL_TIMEOUT);
			if (response.getJson() != null && response.getJson().has("jobs")) {
				JSONArray json = response.getJson().getJSONArray("jobs");
				LOGGER.info("Found {} jobs", new Object[] { json.length() });
				for (int index = 0; index < json.length(); index++) {
					final JSONObject jsonJob = json.getJSONObject(index);
					jobs.add(new Job(site.getId(), jsonJob.getString("name")));
				}
			} else {
				LOGGER.warn("No JSON response came from the server");
			}
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
					applicationLink.getDisplayUrl(), e.getMessage() });
		} catch (JSONException e) {
			LOGGER.error("Failed to get the Job list from {}: {}", applicationLink.getName(), e.getMessage());
		}
		return jobs;
	}

	@Override
	public Job getDetailedJob(Job job) {
		Job result = new Job(job);
		try {
			RestResponse response = executeGetRequest("/job/" + URIUtils.encode(job.getName(),
					"UTF-8") + "/api/json", CALL_TIMEOUT);
			if (response.getJson() != null) {
				result.setDescription(optJsonString(response.getJson(), "description"));
				result.setBuildable(response.getJson().optBoolean("buildable", true));
				result.setInQueue(response.getJson().optBoolean("inQueue"));
				final JSONArray builds = response.getJson().optJSONArray("builds");
				if (builds != null && builds.length() > 0) {
					for (int index = 0; index < builds.length(); index++) {
						final JSONObject jsonBuild = builds.getJSONObject(index);
						final Build build = new Build(result.getId(), jsonBuild.getInt("number"));
						result.getBuilds().add(build);
					}
				}
			} else {
				LOGGER.warn("No JSON response came from the server");
			}
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
					applicationLink.getDisplayUrl(), e.getMessage() });
		} catch (JSONException e) {
			LOGGER.error("Failed to get the Detailed Job {} from {}: {}", new Object[] { job.getName(),
					applicationLink.getName(), e.getMessage() });
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Failed to encode the job name: " + job.getName());
		}
		return result;
	}

	@Override
	public URI getJobUrl(Job job) {
		try {
			return URI.create(applicationLink.getDisplayUrl() + "/job/" + URIUtils.encode(job.getName(), "UTF-8"));
		} catch (UnsupportedEncodingException e) {
			return URI.create(applicationLink.getDisplayUrl() + "/job/" + job.getName());
		}
	}

	@Override
	public Build getDetailedBuild(Job job, Build build) {
		return getDetailedBuild(job, build.getNumber());
	}

	@Override
	public Build getDetailedBuild(Job job, int number) {
		Build build = new Build(job.getId(), number);
		try {
			RestResponse response = executeGetRequest("/job/" + URIUtils.encode(job.getName(),
					"UTF-8") + "/" + number + "/api/json?depth=0", CALL_TIMEOUT);
			if (response.getJson() != null) {
				final JSONObject jsonBuild = response.getJson();
				build.setBuiltOn(optJsonString(jsonBuild, "builtOn"));
				build.setResult(optJsonString(jsonBuild, "result"));
				build.setTimestamp(jsonBuild.getLong("timestamp"));
				build.setDuration(jsonBuild.getLong("duration"));
				final JSONArray actions = jsonBuild.optJSONArray("actions");
				StringBuilder causeBuilder = new StringBuilder();
				if (actions != null) {
					for (int index = 0; index < actions.length(); index++) {
						final JSONObject object = actions.getJSONObject(index);
						if (object.has("causes")) {
							JSONArray causes = object.optJSONArray("causes");
							for (int ii = 0; ii < causes.length(); ii++) {
								final JSONObject cause = causes.getJSONObject(ii);
								if (cause.has("shortDescription") && causeBuilder.indexOf(cause.getString
										("shortDescription")) == -1) {
									if (causeBuilder.length() > 0) {
										causeBuilder.append(", ");
									}
									causeBuilder.append(cause.getString("shortDescription"));
								}
							}
						}
					}
				}
				build.setCause(causeBuilder.toString());
				final JSONArray artifacts = jsonBuild.optJSONArray("artifacts");
				if (artifacts != null && artifacts.length() > 0) {
					for (int index = 0; index < artifacts.length(); index++) {
						final JSONObject artifact = artifacts.getJSONObject(index);
						build.getArtifacts().add(new Artifact(artifact.getString("fileName"),
								artifact.getString("displayPath"), artifact.getString("relativePath")));
					}
				}
				final JSONArray culprits = jsonBuild.optJSONArray("culprits");
				if (culprits != null && culprits.length() > 0) {
					for (int index = 0; index < culprits.length(); index++) {
						final JSONObject culprit = culprits.getJSONObject(index);
						String id = optJsonString(culprit, "id");
						if (id == null && culprit.has("absoluteUrl")) {
							String absoluteUrl = culprit.getString("absoluteUrl");
							id = absoluteUrl.substring(absoluteUrl.lastIndexOf("/"));
						} else if (id == null) {
							id = culprit.getString("fullName");
						}
						build.getCulprits().add(new Culprit(id, culprit.getString("fullName")));
					}
				}
				if (jsonBuild.has("changeSet")) {
					final JSONObject changeSet = jsonBuild.optJSONObject("changeSet");
					if (changeSet != null && changeSet.has("items")) {
						final JSONArray items = changeSet.optJSONArray("items");
						if (items != null && items.length() > 0) {
							for (int index = 0; index < items.length(); index++) {
								final JSONObject item = items.getJSONObject(index);
								String commitId = String.valueOf(index);
								for (String idField : COMMIT_ID_FIELDS) {
									if (item.has(idField) && !item.isNull(idField)) {
										commitId = item.getString(idField);
										break;
									}
								}
								build.getChangeSet().add(new ChangeSet(commitId, optJsonString(item, "msg")));
							}
						}
					}
				}
			} else {
				LOGGER.warn("No JSON response came from the server");
			}
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
					applicationLink.getDisplayUrl(), e.getMessage() });
		} catch (JSONException e) {
			LOGGER.error("Failed to get the Detailed Build {} of {} from {}: {}", new Object[] { number,
					job.getName(), applicationLink.getName(), e.getMessage() });
			LOGGER.debug("JSON Exception", e);
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Failed to encode the job name: " + job.getName());
		}
		return build;
	}

	@Override
	public URI getBuildUrl(Job job, Build build) {
		URI jobUrl = getJobUrl(job);
		return URI.create(jobUrl.toString() + "/" + build.getNumber());
	}

	@Override
	public JobState getJobState(Job job) {
		try {
			RestResponse response = executeGetRequest("/job/" + URIUtils.encode(job.getName(),
					"UTF-8") + "/boq", CALL_TIMEOUT);
			try {
				return JobState.valueOf(response.getResponseBody());
			} catch (Exception e) {
				return JobState.UNKNOWN;
			}
		} catch (CredentialsRequiredException e) {
			LOGGER.error("Authentication failure on {} [{}]: {}", new Object[] { applicationLink.getName(),
					applicationLink.getDisplayUrl(), e.getMessage() });
		} catch (UnsupportedEncodingException e) {
			LOGGER.error("Failed to encode the job name: " + job.getName());
		}
		return JobState.UNKNOWN;
	}

	/**
	 * Execute a Get Request on the {@link ApplicationLink} given
	 *
	 * @param url     the target service url
	 * @param timeout timeout in milliseconds
	 * @return the {@link RestResponse} response
	 * @throws CredentialsRequiredException in case the {@link ApplicationLink} credentials are outdated
	 */
	public RestResponse executeGetRequest(String url, int timeout) throws CredentialsRequiredException {
		return executeGetRequest(applicationLink.createAuthenticatedRequestFactory(), url, timeout);
	}

	/**
	 * Execute a Get Request using the {@link ApplicationLinkRequestFactory} to create the Request
	 *
	 * @param requestFactory the {@link ApplicationLinkRequestFactory}
	 * @param url            the target service url
	 * @param timeout        timeout in milliseconds
	 * @return the {@link RestResponse} response
	 * @throws CredentialsRequiredException in case the {@link ApplicationLink} credentials are outdated
	 */
	private RestResponse executeGetRequest(ApplicationLinkRequestFactory requestFactory, String url,
	                                       int timeout) throws CredentialsRequiredException {
		return executeRequest(requestFactory.createRequest(Request.MethodType.GET, url), requestFactory, timeout);
	}

	/**
	 * Execute the given {@link ApplicationLinkRequest}
	 *
	 * @param request        the {@link ApplicationLinkRequest} to execute
	 * @param requestFactory the {@link ApplicationLinkRequestFactory} used to create the request
	 * @param timeout        timeout in milliseconds
	 * @return the {@link RestResponse} response
	 * @throws CredentialsRequiredException in case the {@link ApplicationLink} credentials are outdated
	 */
	private RestResponse executeRequest(ApplicationLinkRequest request, ApplicationLinkRequestFactory requestFactory,
	                                    int timeout) throws CredentialsRequiredException {
		request.setConnectionTimeout(timeout);
		request.setSoTimeout(timeout);
		return executeRequest(request, requestFactory);
	}

	/**
	 * Execute the given {@link ApplicationLinkRequest}
	 *
	 * @param request        the {@link ApplicationLinkRequest} to execute
	 * @param requestFactory the {@link ApplicationLinkRequestFactory} used to create the request
	 * @return the {@link RestResponse} response
	 * @throws CredentialsRequiredException in case the {@link ApplicationLink} credentials are outdated
	 */
	private RestResponse executeRequest(ApplicationLinkRequest request, ApplicationLinkRequestFactory requestFactory)
			throws CredentialsRequiredException {
		try {
			RestResponse response = request.execute(new ApplicationLinkResponseHandler<RestResponse>() {
				@Override
				public RestResponse credentialsRequired(Response response) throws ResponseException {
					return new CredentialsRequiredRestResponse(response);
				}

				@Override
				public RestResponse handle(Response response) throws ResponseException {
					if (response.getStatusCode() == HttpStatus.SC_FORBIDDEN) {
						return credentialsRequired(response);
					}
					return new RestResponse(response);
				}
			});
			if (response instanceof CredentialsRequiredRestResponse) {
				throw new CredentialsRequiredException(requestFactory, "Unable to authenticate");
			} else {
				return response;
			}
		} catch (ResponseException e) {
			if (e.getCause() instanceof ConnectException) {
				return new RestResponse("Unable to connect to Jenkins. Connection timed out.");
			} else {
				return new RestResponse("Failed to execute request: " + e.getMessage());
			}
		}
	}

	/**
	 * Helper method to get the optional {@link String} element of the given {@link JSONObject} given by the key given
	 *
	 * @param json the {@link JSONObject} to get the {@link String} value from
	 * @param key  the key to get he corresponding value of
	 * @return the {@link String} value, maybe {@code null}
	 */
	private String optJsonString(JSONObject json, String key) {
		if (json.has(key) && !json.isNull(key)) {
			return json.optString(key);
		} else {
			return null;
		}
	}

	/**
	 * Credentials Required specific {@link RestResponse}
	 */
	private class CredentialsRequiredRestResponse extends RestResponse {

		/**
		 * Constructor
		 *
		 * @param response the {@link Response} to base hte CredentialsRequiredRestResponse on
		 */
		private CredentialsRequiredRestResponse(Response response) {
			super(response);
		}

	}

}
