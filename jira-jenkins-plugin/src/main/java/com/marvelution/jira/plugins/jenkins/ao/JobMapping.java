/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.ao;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 * ActiveObjects type for Job Mappings
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
@Preload
public interface JobMapping extends Entity {

	String SITE_ID = "SITE_ID";
	String NAME = "NAME";
	String LAST_BUILD = "LAST_BUILD";
	String LINKED = "LINKED";
	String DELETED = "DELETED";

	int getSiteId();

	void setSiteId(int siteId);

	String getName();

	void setName(String name);

	int getLastBuild();

	void setLastBuild(int lastBuild);

	boolean isLinked();

	void setLinked(boolean linked);

	boolean isDeleted();

	void setDeleted(boolean deleted);

}
