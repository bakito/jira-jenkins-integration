/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jira.plugins.jenkins.applinks;

import com.atlassian.applinks.spi.application.NonAppLinksApplicationType;
import com.atlassian.applinks.spi.application.TypeId;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;

/**
 * Jenkins Application Link Type
 *
 * @author <a href="mailto:markrekveld@marvelution.com">Mark Rekveld</a>
 * @since 1.0.0
 */
public class JenkinsApplicationType extends IconizedIdentifiableType implements NonAppLinksApplicationType {

	private static final String JENKINS = "jenkins";
	public static final TypeId TYPE_ID = new TypeId(JENKINS);

	/**
	 * Constructor
	 *
	 * @param pluginUtil the {@link JenkinsPluginUtil}
	 * @param webResourceUrlProvider the {@link com.atlassian.plugin.webresource.WebResourceUrlProvider} implementation
	 */
	public JenkinsApplicationType(JenkinsPluginUtil pluginUtil, WebResourceUrlProvider webResourceUrlProvider) {
		super(pluginUtil, webResourceUrlProvider);
	}

	@Override
	public String getI18nKey() {
		return JENKINS;
	}

	@Override
	public TypeId getId() {
		return TYPE_ID;
	}

}
