/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package it.com.marvelution.jira.plugins.jenkins.services;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.spi.application.ApplicationIdUtil;
import com.atlassian.applinks.spi.link.ApplicationLinkDetails;
import com.atlassian.applinks.spi.link.MutatingApplicationLinkService;
import com.atlassian.applinks.spi.manifest.ManifestNotFoundException;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.marvelution.jira.plugins.jenkins.applinks.JenkinsApplicationType;
import com.marvelution.jira.plugins.jenkins.model.Site;
import com.marvelution.jira.plugins.jenkins.model.SiteType;
import com.marvelution.jira.plugins.jenkins.services.SiteService;
import com.marvelution.jira.plugins.jenkins.utils.JenkinsPluginUtil;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.net.URI;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * Atlassian wired integration test case for Application actions
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(AtlassianPluginsTestRunner.class)
public class ApplicationLinkIntegrationTest {

	private final URI uri = URI.create("http://localhost:2980/jenkins");
	private final ApplicationId applicationId = ApplicationIdUtil.generate(uri);
	private final SiteService siteService;
	private final MutatingApplicationLinkService applicationLinkService;
	private final WebResourceUrlProvider webResourceUrlProvider;
	private final JenkinsPluginUtil pluginUtil;

	/**
	 * Constructor
	 *
	 * @param siteService the {@link SiteService} from the plugin
	 * @param applicationLinkService the {@link MutatingApplicationLinkService} form JIRA
	 * @param webResourceUrlProvider the {@link WebResourceUrlProvider} from JIRA
	 * @param pluginUtil the {@link JenkinsPluginUtil} from the plugin
	 */
	public ApplicationLinkIntegrationTest(SiteService siteService, MutatingApplicationLinkService applicationLinkService,
	                                      WebResourceUrlProvider webResourceUrlProvider, JenkinsPluginUtil pluginUtil) {
		this.siteService = siteService;
		this.webResourceUrlProvider = webResourceUrlProvider;
		this.applicationLinkService = applicationLinkService;
		this.pluginUtil = pluginUtil;
	}

	/**
	 * Test the adding of a new Jenkins Application Link
	 *
	 * @throws ManifestNotFoundException in case of errors
	 */
	@Test
	public void testAddApplicationLink() throws ManifestNotFoundException {
		ApplicationLinkDetails details = ApplicationLinkDetails.builder().name("Localhost").displayUrl(uri).rpcUrl
				(uri).isPrimary(true).build();
		ApplicationLink applicationLink = applicationLinkService.createApplicationLink(
				new JenkinsApplicationType(pluginUtil, webResourceUrlProvider), details);
		assertThat("ApplicationLink Name assert error", applicationLink.getName(), is("Localhost"));
		assertThat("ApplicationId assert error", applicationLink.getId(), is(applicationId));
		Site site = siteService.getByApplicationLink(applicationLink);
		assertThat("Site ID should be set!", site.getId(), not(0));
		assertThat("Site ApplicationId doesn't need given", site.getApplicationId(), is(applicationId));
		assertThat("Site name assert error", site.getName(), is(details.getName()));
		assertThat("Site type assert error", site.getType(), is(SiteType.JENKINS));
		assertThat("Site display URL assert error", site.getDisplayUrl(),
				is(details.getDisplayUrl().toString()));
	}

	/**
	 * Test the deletion of the Jenkins Application Link created in the add test
	 *
	 * @throws TypeNotInstalledException in case of errors
	 */
	@Test
	public void testDeleteApplicationLink() throws TypeNotInstalledException {
		ApplicationLink link = applicationLinkService.getApplicationLink(applicationId);
		Site site = siteService.getByApplicationLink(link);
		applicationLinkService.deleteApplicationLink(link);
		assertThat("Site with ID " + site.getId() + " should be deleted", siteService.get(site.getId()), nullValue());
	}

}
