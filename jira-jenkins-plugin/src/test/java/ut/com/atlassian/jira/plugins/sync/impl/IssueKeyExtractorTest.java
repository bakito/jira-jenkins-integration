package ut.com.atlassian.jira.plugins.sync.impl;

import com.atlassian.jira.plugins.dvcs.sync.impl.IssueKeyExtractor;
import org.junit.Test;

import java.util.Set;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * Testcase for {@link IssueKeyExtractor}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class IssueKeyExtractorTest {

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with a {@code null} message
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithNullMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys(null);
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(true));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with a {@code empty} message
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithEmptyMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(true));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with a single issue key on a single comment line
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithSingleLineSingleKeyMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("JJI-4");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(false));
		assertThat(keys.size(), is(1));
		assertThat(keys.contains("JJI-4"), is(true));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with a single issue key on a single comment line
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithSingleLineSingleKeyWithCommentMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("Implemented a test case for JJI-4");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(false));
		assertThat(keys.size(), is(1));
		assertThat(keys.contains("JJI-4"), is(true));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with multiple issue keys on a single comment line
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithSingleLineMultipleKeysMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("JJI-4, JJI-1 JJI-3");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(false));
		assertThat(keys.size(), is(3));
		assertThat(keys.contains("JJI-4"), is(true));
		assertThat(keys.contains("JJI-1"), is(true));
		assertThat(keys.contains("JJI-3"), is(true));
		assertThat(keys.contains("JJI-2"), is(false));
	}

	/**
	 * Test for {@link IssueKeyExtractor#extractIssueKeys(String)} with multiple issue keys on multiple comment lines
	 *
	 * @throws Exception
	 */
	@Test
	public void testWithMultipleLinesMultipleKeysMessage() throws Exception {
		Set<String> keys = IssueKeyExtractor.extractIssueKeys("JJI-4\nJJI-1\nJJI-3");
		assertThat(keys, notNullValue());
		assertThat(keys.isEmpty(), is(false));
		assertThat(keys.size(), is(3));
		assertThat(keys.contains("JJI-4"), is(true));
		assertThat(keys.contains("JJI-1"), is(true));
		assertThat(keys.contains("JJI-3"), is(true));
		assertThat(keys.contains("JJI-2"), is(false));
	}

}
