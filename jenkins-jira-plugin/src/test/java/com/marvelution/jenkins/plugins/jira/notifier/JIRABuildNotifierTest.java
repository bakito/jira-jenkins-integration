/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.notifier;

import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import hudson.Launcher;
import hudson.model.AbstractBuild;
import hudson.model.BuildListener;
import org.apache.commons.httpclient.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.PrintStream;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

/**
 * Testcase for the {@link JIRABuildNotifier} implementation
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@RunWith(MockitoJUnitRunner.class)
public class JIRABuildNotifierTest {

	@Mock
	private AbstractBuild build;
	@Mock
	private Launcher launcher;
	@Mock
	private BuildListener listener;
	@Mock
	private WebResource webResource;
	@Mock
	private ClientResponse response;
	@Mock
	private PrintStream logger;
	private JIRABuildNotifier notifier;

	/**
	 * Setup the {@link JIRABuildNotifier} for the tests
	 */
	@Before
	public void setup() {
		notifier = new JIRABuildNotifier("http://localhost:2990/jira/rest/jenkins/1.0/job/1/sync") {
			@Override
			WebResource getResource() {
				return webResource;
			}
		};
		when(listener.getLogger()).thenReturn(logger);
	}

	/**
	 * Test {@link JIRABuildNotifier#perform(hudson.model.AbstractBuild, hudson.Launcher,
	 * hudson.model.BuildListener)} with no post URL configured
	 *
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testNoPostUrl() throws Exception {
		notifier = new JIRABuildNotifier("");
		assertThat(notifier.perform(build, launcher, listener), is(true));
		verifyZeroInteractions(webResource);
		verifyZeroInteractions(logger);
		verify(listener).error(any(String.class), any(String.class));
	}

	/**
	 * Test {@link JIRABuildNotifier#perform(hudson.model.AbstractBuild, hudson.Launcher,
	 * hudson.model.BuildListener)} which is successfully to connect to JIRA
	 *
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testSuccessfulTrigger() throws Exception {
		when(webResource.post(ClientResponse.class)).thenReturn(response);
		when(response.getStatus()).thenReturn(HttpStatus.SC_OK);
		assertThat(notifier.perform(build, launcher, listener), is(true));
		verify(listener).getLogger();
		verify(logger).printf("%s triggered JIRA to update builds of this job\n", "Successfully");
	}

	/**
	 * Test {@link JIRABuildNotifier#perform(hudson.model.AbstractBuild, hudson.Launcher,
	 * hudson.model.BuildListener)} which is unable to connect to JIRA
	 *
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testUnableToTrigger() throws Exception {
		when(webResource.post(ClientResponse.class)).thenReturn(response);
		when(response.getStatus()).thenReturn(HttpStatus.SC_INTERNAL_SERVER_ERROR);
		assertThat(notifier.perform(build, launcher, listener), is(true));
		verify(listener).getLogger();
		verify(logger).printf("%s triggered JIRA to update builds of this job\n", "Unable to");
	}

	/**
	 * Test {@link JIRABuildNotifier#perform(hudson.model.AbstractBuild, hudson.Launcher,
	 * hudson.model.BuildListener)} which is failed to connect to JIRA
	 *
	 * @throws Exception in case of test errors
	 */
	@Test
	public void testFailedTrigger() throws Exception {
		when(webResource.post(ClientResponse.class)).thenThrow(new ClientHandlerException("I failed you"));
		assertThat(notifier.perform(build, launcher, listener), is(true));
		verify(listener, times(0)).getLogger();
		verify(listener).error("Failed to triggered JIRA to update builds of this job -> %s", "I failed you");
	}

}
