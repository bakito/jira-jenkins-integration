/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.applinks;

import hudson.model.Hudson;
import hudson.security.GlobalMatrixAuthorizationStrategy;
import org.junit.Before;
import org.junit.Rule;
import org.jvnet.hudson.test.JenkinsRule;
import org.w3c.css.sac.CSSParseException;
import org.w3c.css.sac.ErrorHandler;

import java.io.IOException;

/**
 * Base testcase for actions
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public abstract class BaseActionTest {

	@Rule
	public JenkinsRule jenkins = new JenkinsRule();

	/**
	 * Setup the {@link #jenkins} rule with the correct security realm and authentication strategy
	 */
	@Before
	public void setup() throws IOException {
		jenkins.jenkins.setSecurityRealm(jenkins.createDummySecurityRealm());
		GlobalMatrixAuthorizationStrategy authorizationStrategy = new GlobalMatrixAuthorizationStrategy();
		authorizationStrategy.add(Hudson.ADMINISTER, "admin");
		jenkins.jenkins.setAuthorizationStrategy(authorizationStrategy);
	}

	/**
	 * Get a {@link JenkinsRule.WebClient}
	 *
	 * @return the {@link JenkinsRule.WebClient}
	 */
	protected JenkinsRule.WebClient getWebClient() {
		JenkinsRule.WebClient webClient = jenkins.createWebClient();
		webClient.setThrowExceptionOnFailingStatusCode(false);
		webClient.setCssErrorHandler(new ErrorHandler() {
			@Override
			public void warning(CSSParseException e) {
			}

			@Override
			public void error(CSSParseException e) {
			}

			@Override
			public void fatalError(CSSParseException e) {
			}
		});
		webClient.setJavaScriptEnabled(false);
		webClient.setPrintContentOnFailingStatusCode(false);
		return webClient;
	}

}
