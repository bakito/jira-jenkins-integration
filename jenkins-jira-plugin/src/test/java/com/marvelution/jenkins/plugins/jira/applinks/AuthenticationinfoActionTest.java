/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.applinks;

import com.gargoylesoftware.htmlunit.Page;
import org.junit.Test;
import org.jvnet.hudson.test.JenkinsRule;

import javax.servlet.http.HttpServletResponse;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Testcase for {@link com.marvelution.jenkins.plugins.jira.applinks.AuthenticationinfoAction}
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class AuthenticationinfoActionTest extends BaseActionTest {

	private static final String MARVELUTION_ISSUES_ID = "27274ce6-d606-3a64-b0bb-df07fa4b4d00";
	private static final String MARVELUTION_ISSUES_URL = "https://marvelution.atlassian.net";

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.AuthenticationinfoAction#handle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testAnonymousCall() throws Exception {
		JenkinsRule.WebClient webClient = getWebClient();
		Page page = webClient.goTo("rest/applinks/1.0/authenticationinfo", "");
		assertThat(page.getWebResponse().getStatusCode(), is(HttpServletResponse.SC_UNAUTHORIZED));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.AuthenticationinfoAction#handle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testAuthenticatedCallWithNoIdOrUrlGiven() throws Exception {
		JenkinsRule.WebClient webClient = getWebClient();
		webClient.login("admin");
		Page page = webClient.goTo("rest/applinks/1.0/authenticationinfo", "");
		assertThat(page.getWebResponse().getStatusCode(), is(HttpServletResponse.SC_OK));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.AuthenticationinfoAction#handle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testAuthenticatedCallWithIdAndUrl() throws Exception {
		JenkinsRule.WebClient webClient = getWebClient();
		webClient.login("admin");
		Page page = webClient.goTo("rest/applinks/1.0/authenticationinfo/id/" + MARVELUTION_ISSUES_ID + "/url/" +
				MARVELUTION_ISSUES_URL, "");
		assertThat(page.getWebResponse().getStatusCode(), is(HttpServletResponse.SC_OK));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.AuthenticationinfoAction#handle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 *
	 * Special test for https://marvelution.atlassian.net/browse/JJI-35
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testAuthenticatedCallWithIdAndUrlFormatError() throws Exception {
		JenkinsRule.WebClient webClient = getWebClient();
		webClient.login("admin");
		Page page = webClient.goTo("rest/applinks/1.0/authenticationinfo/id/" + MARVELUTION_ISSUES_ID +
				"/url/https:/marvelution.atlassian.net",
				"");
		assertThat(page.getWebResponse().getStatusCode(), is(HttpServletResponse.SC_OK));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.AuthenticationinfoAction#handle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testAuthenticatedCallWithIdAndUrlButNotFound() throws Exception {
		JenkinsRule.WebClient webClient = getWebClient();
		webClient.login("admin");
		Page page = webClient.goTo("rest/applinks/1.0/authenticationinfo/id/FAKE.ID/url/" + MARVELUTION_ISSUES_URL, "");
		assertThat(page.getWebResponse().getStatusCode(), is(HttpServletResponse.SC_NOT_FOUND));
	}

	/**
	 * Test {@link com.marvelution.jenkins.plugins.jira.applinks.AuthenticationinfoAction#handle(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 *
	 * @throws Exception in case of errors
	 */
	@Test
	public void testAuthenticatedCallWithIdAndInvalidUrl() throws Exception {
		JenkinsRule.WebClient webClient = getWebClient();
		webClient.login("admin");
		Page page = webClient.goTo("rest/applinks/1.0/authenticationinfo/id/FAKE.ID/url/FAKE.URL", "");
		assertThat(page.getWebResponse().getStatusCode(), is(HttpServletResponse.SC_NOT_FOUND));
	}

}
