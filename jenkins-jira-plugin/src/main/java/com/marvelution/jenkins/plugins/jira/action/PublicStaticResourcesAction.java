/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.action;

import com.marvelution.jenkins.plugins.jira.JIRAPlugin;
import hudson.Extension;
import hudson.model.UnprotectedRootAction;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URL;

/**
 * {@link UnprotectedRootAction} to handle unprotected static resources
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@Extension
public class PublicStaticResourcesAction implements UnprotectedRootAction {

	@Override
	public String getIconFileName() {
		return null;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getUrlName() {
		return JIRAPlugin.getPluginShortName();
	}

	/**
	 * Dynamically handle the {@link StaplerRequest}
	 *
	 * @param request  the {@link StaplerRequest} to handle
	 * @param response the {@link StaplerResponse} to respond in
	 * @throws IOException      in case of IO errors
	 * @throws ServletException in case of Servlet Errors
	 */
	public void doDynamic(StaplerRequest request, StaplerResponse response) throws IOException, ServletException {
		response.setHeader("Cache-Control", "public, s-maxage=86400");
		if (JIRAPlugin.getPluginWrapper() == null) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			return;
		}
		String path = request.getRestOfPath();
		if (path.length() == 0) {
			path = "/";
		}
		if (path.contains("..") || path.length() < 1) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST);
			return;
		}
		response.serveLocalizedFile(request, new URL(JIRAPlugin.getPluginWrapper().baseResourceURL, '.' + path),
				86400000);
	}

}
