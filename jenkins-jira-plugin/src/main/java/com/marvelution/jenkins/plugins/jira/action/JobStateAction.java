/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.action;

import hudson.model.*;
import hudson.security.AccessDeniedException2;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * {@link Action} to provide the Job state
 *
 * @author Mark Rekveld
 * @since 1.1.1
 */
public class JobStateAction implements Action {

	public static final String URL_NAME = "state";
	private final AbstractProject target;

	/**
	 * Constructor
	 *
	 * @param target the {@link AbstractProject} target of this action
	 */
	public JobStateAction(AbstractProject target) {
		this.target = target;
	}

	@Override
	public String getIconFileName() {
		return null;
	}

	@Override
	public String getDisplayName() {
		return null;
	}

	@Override
	public String getUrlName() {
		return URL_NAME;
	}

	/**
	 * Dynamic method implementation that is called then this action is visited.
	 *
	 * @param request the {@link org.kohsuke.stapler.StaplerRequest}
	 * @param response the {@link org.kohsuke.stapler.StaplerResponse}
	 * @throws java.io.IOException in case of IO errors
	 * @throws javax.servlet.ServletException in case of Servlet Errors
	 */
	@SuppressWarnings("unchecked")
	public void doDynamic(final StaplerRequest request, final StaplerResponse response) throws
			IOException, ServletException {
		if (target.hasPermission(Item.READ)) {
			String state = "IDLE";
			if (target.isInQueue()) {
				state = "QUEUED";
			} else {
				for (Object build : target.getBuilds()) {
					if (build instanceof Run && ((Run) build).isBuilding()) {
						state = "BUILDING";
						break;
					}
				}
			}
			response.setStatus(HttpServletResponse.SC_OK);
			response.getWriter().write(state);
		} else {
			throw new AccessDeniedException2(Hudson.getAuthentication(), Item.READ);
		}
	}

}
