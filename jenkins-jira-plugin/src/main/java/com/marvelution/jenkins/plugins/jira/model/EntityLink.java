/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.model;

import com.google.common.base.Objects;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Entity Link Model
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@XStreamAlias("entityLink")
public class EntityLink {

	@XStreamAlias("applicationId")
	private String applicationId;
	@XStreamAlias("typeId")
	private String typeId;
	@XStreamAlias("key")
	private String key;
	@XStreamAlias("name")
	private String name;
	@XStreamAlias("isPrimary")
	private Boolean primary;

	public String getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}

	public String getTypeId() {
		return typeId;
	}

	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getPrimary() {
		return primary;
	}

	public void setPrimary(Boolean primary) {
		this.primary = primary;
	}

	@Override
	public String toString() {
		return Objects.toStringHelper(this)
				.add("applicationId", applicationId)
				.add("typeId", typeId)
				.add("key", key)
				.toString();
	}

}
