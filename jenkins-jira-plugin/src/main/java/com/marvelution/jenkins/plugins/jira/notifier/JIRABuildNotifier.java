/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.notifier;

import com.marvelution.jenkins.plugins.jira.utils.PluginUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import hudson.Extension;
import hudson.Launcher;
import hudson.model.AbstractBuild;
import hudson.model.AbstractProject;
import hudson.model.Api;
import hudson.model.BuildListener;
import hudson.tasks.BuildStepDescriptor;
import hudson.tasks.BuildStepMonitor;
import hudson.tasks.Notifier;
import hudson.tasks.Publisher;
import org.kohsuke.stapler.DataBoundConstructor;
import org.kohsuke.stapler.export.Exported;
import org.kohsuke.stapler.export.ExportedBean;

import java.io.IOException;
import java.net.URI;

/**
 * Custom {@link Notifier} implementation to notify build completions to JIRA
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
@ExportedBean
public class JIRABuildNotifier extends Notifier {

	@Extension
	public static final JIRADBuildNotifierDescriptor DESCRIPTOR = new JIRADBuildNotifierDescriptor();
	@Exported
	public final String postUrl;

	/**
	 * Constructor
	 *
	 * @param postUrl the URL to POST to when the is completed
	 */
	@DataBoundConstructor
	public JIRABuildNotifier(String postUrl) {
		this.postUrl = URI.create(postUrl).toString();
	}

	@Override
	public BuildStepMonitor getRequiredMonitorService() {
		return BuildStepMonitor.NONE;
	}

	@Override
	public boolean perform(AbstractBuild<?, ?> build, Launcher launcher, BuildListener listener) throws
			InterruptedException, IOException {
		if (postUrl != null) {
			try {
				ClientResponse response = getResource().post(ClientResponse.class);
				listener.getLogger().printf("%s triggered JIRA to update builds of this job\n",
						((response.getStatus() / 100 == 2)? "Successfully"	: "Unable to"));
			} catch (Exception e) {
				listener.error("Failed to triggered JIRA to update builds of this job -> %s", e.getMessage());
			}
		}
		return true;
	}

	/**
	 * Getter for the {@link WebResource}
	 *
	 * @return the {@link WebResource}
	 */
	/* package */ WebResource getResource() {
		return new Client().resource(postUrl);
	}

	/**
	 * getter for the {@link Api} object
	 *
	 * @return the {@link Api} object
	 */
	public final Api getApi() {
		return new Api(this);
	}

	@Override
	public BuildStepDescriptor getDescriptor() {
		return DESCRIPTOR;
	}

	/**
	 * {@link JIRABuildNotifier} descriptor class
	 */
	public static class JIRADBuildNotifierDescriptor extends BuildStepDescriptor<Publisher> {

		@Override
		public boolean isApplicable(Class<? extends AbstractProject> aClass) {
			return true;
		}

		@Override
		public String getDisplayName() {
			return "JIRA Build Notifier";
		}

		/**
		 * Getter for the base of all the help urls
		 *
		 * @return the base help url
		 */
		public String getBaseHelpURL() {
			return "/plugin/" + PluginUtils.getPluginArifactId() + "/static/";
		}

	}

}
