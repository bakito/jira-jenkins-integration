/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.store;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import com.marvelution.jenkins.plugins.jira.model.EntityLink;
import hudson.model.AbstractProject;

import java.io.IOException;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Entity Link Store
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class EntityLinkStore extends BaseStore {

	private final List<EntityLink> entityLinks = Lists.newArrayList();

	private EntityLinkStore(AbstractProject project) {
		super(project.getRootDir());
	}

	public static EntityLinkStore getStore(AbstractProject project) throws IOException {
		EntityLinkStore store = new EntityLinkStore(project);
		store.load();
		return store;
	}

	public void add(EntityLink entityLink) throws IOException {
		entityLinks.add(entityLink);
		save();
	}

	public void remove(final String typeId, final String key, final String applicationId) throws IOException {
		try {
			EntityLink link = Iterables.find(entityLinks, new Predicate<EntityLink>() {
				@Override
				public boolean apply(EntityLink entityLink) {
					return entityLink.getTypeId().equals(typeId) && entityLink.getKey().equals(key) && entityLink
							.getApplicationId().equals(applicationId);
				}
			});
			if (link != null) {
				entityLinks.remove(link);
				save();
			}
		} catch (NoSuchElementException e) {
			// Ignore
		}
	}

}
