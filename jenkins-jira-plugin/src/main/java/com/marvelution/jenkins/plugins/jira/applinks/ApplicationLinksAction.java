/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.applinks;

import hudson.ExtensionList;
import hudson.ExtensionPoint;
import hudson.model.Hudson;
import hudson.security.AccessControlled;
import hudson.security.Permission;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Jenkins ExtensionPoint for Application Link Actions
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public abstract class ApplicationLinksAction implements ExtensionPoint {

	/**
	 * Getter for all Implementations of the {@link ApplicationLinksAction} {@link ExtensionPoint}s
	 *
	 * @return {@link ExtensionList}
	 */
	public static ExtensionList<ApplicationLinksAction> all() {
		return Hudson.getInstance().getExtensionList(ApplicationLinksAction.class);
	}

	/**
	 * Check if the current user has the given {@link Permission} on the {@link AccessControlled} object given
	 *
	 * @param ac         the {@link AccessControlled} object
	 * @param permission the {@link Permission}
	 * @param <AC>
	 * @return {@code true} if the user has the {@link Permission}, {@code false} otherwise
	 */
	protected <AC extends AccessControlled> boolean hasPermission(AC ac, Permission permission) {
		return ac.getACL().hasPermission(Hudson.getAuthentication(), permission);
	}

	/**
	 * Check if the given action string is supported by the {@link ApplicationLinksAction} implementation
	 *
	 * @param action the action string to check
	 * @return {@code true} if action is supported, {@code false} otherwise
	 */
	public abstract boolean canHandle(String action);

	/**
	 * Handle the Application Links Action
	 *
	 * @param request the {@link HttpServletRequest}
	 * @param response the {@link HttpServletResponse}
	 * @throws Exception in case of errors
	 */
	public abstract void handle(HttpServletRequest request, HttpServletResponse response) throws Exception;

}
