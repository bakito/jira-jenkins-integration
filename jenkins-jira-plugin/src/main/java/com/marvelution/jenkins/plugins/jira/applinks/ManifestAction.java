/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.applinks;

import com.marvelution.jenkins.plugins.jira.model.Manifest;
import com.marvelution.jenkins.plugins.jira.utils.XStreamUtils;
import hudson.Extension;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.net.URI;

/**
 * Handler for the {@code /rest/applinks/1.0/manifest} request from JIRA
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@Extension
public class ManifestAction extends ApplicationLinksAction {

	public static final String ACTION = "manifest";

	@Override
	public boolean canHandle(String action) {
		return action.startsWith(ACTION);
	}

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
		StringBuilder url = new StringBuilder();
		String scheme = request.getScheme();
		url.append(scheme).append("://").append(request.getServerName());
		int port = request.getServerPort();
		if (("http".equalsIgnoreCase(scheme) && port != 80) || ("https".equalsIgnoreCase(scheme) && port != 443)) {
			url.append(":").append(port);
		}
		url.append(request.getContextPath()).append("/");
		XStreamUtils.writeXmlToResponse(Manifest.create(URI.create(url.toString())), response);
	}

}
