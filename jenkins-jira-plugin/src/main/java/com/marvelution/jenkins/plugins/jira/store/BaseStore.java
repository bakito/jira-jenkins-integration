/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.store;

import com.marvelution.jenkins.plugins.jira.JIRAPlugin;
import hudson.XmlFile;
import hudson.model.Hudson;

import java.io.File;
import java.io.IOException;

/**
 * Base Store
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class BaseStore {

	private final transient File rootDir;

	/**
	 * Constructor
	 *
	 * @param rootDir the root directory to place the config {@link File} in
	 */
	protected BaseStore(File rootDir) {
		this.rootDir = rootDir;
	}

	/**
	 * Load the Store from the configuration {@link File}
	 *
	 * @throws IOException in case of load errors
	 */
	protected void load() throws IOException {
		XmlFile xml = getConfigXml();
		if (xml.exists()) {
			xml.unmarshal(this);
		}
	}

	/**
	 * Save the Store to the configuration {@link File}
	 *
	 * @throws IOException in case of save errors
	 */
	protected void save() throws IOException {
		XmlFile config = getConfigXml();
		config.write(this);
	}

	/**
	 * Getter for the {@link XmlFile}
	 *
	 * @return the {@link XmlFile}
	 */
	protected XmlFile getConfigXml() {
		return new XmlFile(Hudson.XSTREAM, new File(rootDir, JIRAPlugin.getPluginShortName() + ".xml"));
	}

}
