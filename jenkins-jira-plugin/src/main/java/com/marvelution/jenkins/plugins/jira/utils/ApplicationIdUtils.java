/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.utils;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.UUID;

/**
 * ApplicationId utility class
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class ApplicationIdUtils {

	/**
	 * Get an ApplicationId from the given {@link URI}
	 *
	 * @param url the {@link URI} to get an Id for
	 * @return the ApplicationId;
	 */
	public static String getApplicationId(URI url) {
		String normalisedUri = url.normalize().toASCIIString();
		while ((normalisedUri.endsWith("/")) && (normalisedUri.length() > 1)) {
			normalisedUri = normalisedUri.substring(0, normalisedUri.length() - 1);
		}
		return UUID.nameUUIDFromBytes(normalisedUri.getBytes()).toString();
	}

	/**
	 * Get an ApplicationId from the given {@link URL}
	 *
	 * @param url the {@link URL} to get an Id for
	 * @return the ApplicationId, may be {@code null}
	 */
	public static String getApplicationId(URL url) {
		try {
			return getApplicationId(url.toURI());
		} catch (URISyntaxException e) {
			return null;
		}
	}

}
