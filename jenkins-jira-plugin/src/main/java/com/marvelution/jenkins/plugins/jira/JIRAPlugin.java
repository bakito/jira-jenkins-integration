/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira;

import com.marvelution.jenkins.plugins.jira.filter.ApplinksServletFilter;
import hudson.Plugin;
import hudson.PluginWrapper;
import hudson.util.PluginServletFilter;
import org.kohsuke.stapler.StaplerRequest;
import org.kohsuke.stapler.StaplerResponse;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The {@link Plugin} implementation for the JIRA Jenkins Plugin.
 * <p/>
 * This will register the {@link ApplinksServletFilter}
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class JIRAPlugin extends Plugin {

	private static final Logger LOGGER = Logger.getLogger(JIRAPlugin.class.getName());
	private transient ApplinksServletFilter applinksServletFilter;
	private static JIRAPlugin self;

	/**
	 * Constructor that initializes the {@link ApplinksServletFilter} service
	 */
	public JIRAPlugin() {
		applinksServletFilter = new ApplinksServletFilter();
	}

	@Override
	public void start() throws Exception {
		super.start();
		LOGGER.info("Adding the ApplicationLink ServletFilter to handle related REST requests");
		PluginServletFilter.addFilter(applinksServletFilter);
		self = this;
	}

	@Override
	public void stop() throws Exception {
		self = null;
		LOGGER.info("Removing the ApplicationLink ServletFilter");
		PluginServletFilter.removeFilter(applinksServletFilter);
		super.stop();
	}

	/**
	 * Handle all calls to {@code /plugin/jenkins-jira-plugin}
	 *
	 * @param request  the {@link StaplerRequest}
	 * @param response the {@link StaplerResponse}
	 * @throws IOException      in case of IO errors
	 * @throws ServletException in case of servlet errors
	 */
	public void doDynamic(StaplerRequest request, StaplerResponse response) throws IOException, ServletException {
		response.setStatus(HttpServletResponse.SC_OK);
	}

	/**
	 * Getter for the plugin short name
	 *
	 * @return the plugin short name
	 */
	public static String getPluginShortName() {
		return self.getWrapper().getShortName();
	}

	/**
	 * Getter for the {@link PluginWrapper}
	 *
	 * @return the {@link PluginWrapper}
	 */
	public static PluginWrapper getPluginWrapper() {
		return self.getWrapper();
	}

}
