/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.filter;

import com.marvelution.jenkins.plugins.jira.JIRAPlugin;
import com.marvelution.jenkins.plugins.jira.applinks.ApplicationLinksAction;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * {@link Filter} to response to the application link requests
 *
 * @author Mark Rekveld
 * @since 1.0.0
 */
public class ApplinksServletFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(ApplinksServletFilter.class.getName());
	private static final String AUTH_CONF_URL = "/plugins/servlet/applinks/auth/conf/";
	public static final String REST_APPLINKS_URL = "/rest/applinks/1.0/";
	private ServletContext servletContext;

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		servletContext = filterConfig.getServletContext();
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException,
			ServletException {
		if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
			String pathInfo = ((HttpServletRequest) request).getPathInfo();
			if (pathInfo.startsWith(REST_APPLINKS_URL)) {
				String action = pathInfo.substring(REST_APPLINKS_URL.length());
				LOGGER.info("applinks action: " + ((HttpServletRequest) request).getMethod() + " " + action);
				for (ApplicationLinksAction linksAction : ApplicationLinksAction.all()) {
					if (linksAction.canHandle(action)) {
						try {
							linksAction.handle((HttpServletRequest) request, (HttpServletResponse) response);
						} catch (Exception e) {
							LOGGER.log(Level.SEVERE, "Failed to serve applinks action: " + action, e);
							((HttpServletResponse) response).setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
						}
						return;
					}
				}
			} else if (pathInfo.startsWith(AUTH_CONF_URL)) {
				RequestDispatcher dispatcher = servletContext.getRequestDispatcher("/" + JIRAPlugin
						.getPluginShortName() + "/static/authConf.html");
				dispatcher.forward(request, response);
				return;
			} else if (pathInfo.startsWith("/plugin/" + JIRAPlugin.getPluginShortName() + "/static")) {
				RequestDispatcher dispatcher = servletContext.getRequestDispatcher(pathInfo.substring(pathInfo
						.indexOf("/", 2)));
				dispatcher.forward(request, response);
				return;
			}
		}
		chain.doFilter(request, response);
	}

}
