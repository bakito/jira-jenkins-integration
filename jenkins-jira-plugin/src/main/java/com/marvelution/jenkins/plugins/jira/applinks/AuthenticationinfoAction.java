/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.applinks;

import com.marvelution.jenkins.plugins.jira.filter.ApplinksServletFilter;
import com.marvelution.jenkins.plugins.jira.model.Manifest;
import com.marvelution.jenkins.plugins.jira.utils.XStreamUtils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import hudson.Extension;
import hudson.model.Hudson;
import org.apache.commons.lang.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Handler for the {@code /rest/applinks/1.0/authenticationinfo/*} request from JIRA
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
@Extension
public class AuthenticationinfoAction extends ApplicationLinksAction {

	private static final Logger LOGGER = Logger.getLogger(AuthenticationinfoAction.class.getName());
	private static final Pattern PATTERN = Pattern.compile("(.*?)/id/(.*?)/url/(.*?$)");
	public static final String ACTION = "authenticationinfo";

	@Override
	public boolean canHandle(String action) {
		return action.startsWith(ACTION);
	}

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response) throws Exception {
		int status = HttpServletResponse.SC_UNAUTHORIZED;
		if (hasPermission(Hudson.getInstance(), Hudson.ADMINISTER)) {
			status = HttpServletResponse.SC_OK;
			String path = request.getPathInfo();
			Matcher matcher = PATTERN.matcher(path);
			LOGGER.info(path);
			if (matcher.matches()) {
				String appId = matcher.group(2);
				String remoteUrl = path.substring(matcher.start(3));
				if (!remoteUrl.contains("://") && remoteUrl.contains(":/")) {
					// Don't know how this happens but the a slash was removed to restore this!
					remoteUrl = StringUtils.replaceOnce(remoteUrl, ":/", "://");
				}
				try {
					WebResource webResource = getManifestResource(remoteUrl);
					LOGGER.info("Verifying manifest for " + webResource.getURI().toASCIIString());
					ClientResponse clientResponse = webResource.accept(MediaType.APPLICATION_XML_TYPE).get
							(ClientResponse.class);
					Manifest remoteManifest = XStreamUtils.getEntityFromRequest(clientResponse.getEntityInputStream(),
							Manifest.class);
					if (remoteManifest == null || !remoteManifest.getId().equals(appId)) {
						status = HttpServletResponse.SC_NOT_FOUND;
					}
				} catch (Exception e) {
					LOGGER.log(Level.SEVERE, "Failed to verify the manifest on remote: " + remoteUrl + " with error: "
							+ e.getMessage());
					status = HttpServletResponse.SC_NOT_FOUND;
				}
			}
		}
		response.setStatus(status);
	}

	/**
	 * Get a {@link WebResource} to the Manifest REST resource
	 *
	 * @param remoteUrl the base URL of the remote Application Link system
	 * @return the {@link WebResource}
	 */
	/* package */ WebResource getManifestResource(String remoteUrl) {
		return new Client().resource(remoteUrl).path(ApplinksServletFilter
				.REST_APPLINKS_URL).path("manifest");
	}

}
