/*
 * Licensed to Marvelution under one or more contributor license
 * agreements.  See the NOTICE file distributed with this work
 * for additional information regarding copyright ownership.
 * Marvelution licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.marvelution.jenkins.plugins.jira.store;

import com.google.common.collect.Maps;
import com.marvelution.jenkins.plugins.jira.model.ApplicationLink;
import hudson.model.Hudson;

import java.io.IOException;
import java.util.Map;

/**
 * Store for {@link ApplicationLink}s
 *
 * @author Mark Rekveld
 * @since 1.1.0
 */
public class ApplicationLinkStore extends BaseStore {

	private static transient ApplicationLinkStore STORE;
	private final Map<String, ApplicationLink> applinks = Maps.newHashMap();

	/**
	 * Private constructor
	 */
	private ApplicationLinkStore() {
		super(Hudson.getInstance().getRootDir());
	}

	/**
	 * Getter for the Store instance
	 *
	 * @return the {@link ApplicationLinkStore}
	 * @throws IOException in case of store load errors
	 */
	public static ApplicationLinkStore getStore() throws IOException {
		if (STORE == null) {
			STORE = new ApplicationLinkStore();
			STORE.load();
		}
		return STORE;
	}

	/**
	 * Get an {@link ApplicationLink} by its Id
	 *
	 * @param appId the Application Id
	 * @return the {@link ApplicationLink}, may be {@code null}
	 */
	public ApplicationLink get(String appId) {
		return applinks.get(appId);
	}

	/**
	 * Add a new {@link ApplicationLink}
	 *
	 * @param appId           the Application Id, and should match the Id in the {@link ApplicationLink}
	 * @param applicationLink the {@link ApplicationLink}
	 * @throws IOException in case of store save errors
	 */
	public void add(String appId, ApplicationLink applicationLink) throws IOException {
		if (appId.equals(applicationLink.getId())) {
			applinks.put(appId, applicationLink);
			save();
		} else {
			throw new IllegalArgumentException("ApplicationId " + appId + " doesn't match ApplicationLink Id " +
					applicationLink.getId());
		}
	}

	/**
	 * Remove the {@link ApplicationLink} by its Id
	 *
	 * @param appId the ApplicationLink Id to remove
	 * @throws IOException in case of store save errors
	 */
	public void remove(String appId) throws IOException {
		if (applinks.containsKey(appId)) {
			applinks.remove(appId);
			save();
		}
	}

}
