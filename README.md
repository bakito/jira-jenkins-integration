Homepage/Wiki
=============
<https://marvelution.atlassian.net/wiki/display/JJI>

Issue Tracker
=============
<https://marvelution.atlassian.net/browse/JJI>

Continuous Builder
==================
<https://marvelution.atlassian.net/builds/browse/JJI>

License
=======
[The Apache Software License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.txt)
